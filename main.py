# -*- coding: UTF-8  -*-
# @time     : 2023-07-31 14:55
# @Author   : Zhong Xue
# @File     : main.py
import sys
import os
import pytest

sys.path.append(os.path.dirname(__file__))

from common.mySetting import reportPath, testResultPath

if __name__ == '__main__':
    pytest.main(['-s','--reruns','1',"--alluredir", reportPath, "--clean-alluredir"])

    # 以下代码可从跑上次失败的测试用例
    # pytest.main(['-s','--lf','--isCopy=false', "--alluredir", reportPath])

    # 生成allure报告
    # os.system('allure generate %s -o %s --clean ' % (reportPath,testResultPath))  # 生成allure报告
    # os.system('allure generate %s  --clean ' % reportPath)  # 生成allure报告

    # 执行paper-clip.exe文件
    # paper_clip()

    # 启动allure报告
    # os.system("allure serve %s" % reportPath)
