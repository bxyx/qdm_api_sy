# -*- coding: UTF-8  -*-
# @time     : 2023-09-13 11:49
# @Author   : Zhong Xue
# @File     : base_standard_management.py
from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr, executeJavaScript, ranInt, getPassTime, getDeltaTime


class BaseStandardManagement(CommonAPI):
    """试验规程管理"""

    def addOrUpdateStandardType(self, userId, idAdd=True, projectID=current_env['projectID']):
        """新增或编辑规程类型"""
        # 请求参数
        data = {
            "OpUserId": userId,
            "IsAdd": idAdd,
            "StandarType":{
                "Id": executeJavaScript('getGuid'),
                "TypeName": ranStr(5),
                "OrderCode": ranInt(2),
                "Remark": ranStr(5)
            },
            "DataType": 1,  # 1试验 2质检
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIAddOrUpdateStandardType,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getStandardTypeList(self,projectID=current_env['projectID']):
        """获取规程类型列表"""

        # 请求参数
        data = {
            "Search":"",
            "DataType":1,
            "Pagination":{"page":1,"rows":2000},
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetStandardTypeList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deleteStandardType(self, userId, standardTypeId, projectID=current_env['projectID']):
        """删除规程类型"""
        # 请求参数
        data = {
            "OpUserId": userId,
            "StandarTypeId": standardTypeId,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIDeleteStandardType,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def addStandardInfo(self, typeName, standardTypeId, projectID=current_env['projectID']):
        """添加规程信息"""
        # 请求参数
        data = {
            "ID": executeJavaScript('getGuid'),
            "StandardName": ranStr(5),
            "PublishCode": ranStr(5),
            "ExecuteDate": getPassTime(365).split(' ')[0],
            "ExpireDate": getDeltaTime(365).split(' ')[0],
            "IsSystem": True,
            "IsDefault": False,
            "TypeName": typeName,
            "StandardTypeId": standardTypeId,
            "Remark": None,
            "IsUploadFile": False,
            "DataType": 1,
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIAddStandardInfo,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getStandardList(self, standardTypeId, keyWord, projectID=current_env['projectID']):
        """获取规程列表"""
        # 请求参数
        data = {
            "rows": 10,
            "page": 1,
            "standardTypeId": standardTypeId,
            "DataType": 1,
            "keyWord": keyWord,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetStandardList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deleteStandardById(self,*args):
        """删除标准信息"""
        # 请求参数
        data = list(args)

        # 发送请求
        resp = self.request(self.APIDeleteStandardById,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp



baseStandardManagementObj = BaseStandardManagement()
if __name__ == '__main__':
    baseStandardManagementObj.addOrUpdateStandardType("e5291001-2b37-41ea-bfda-d85167404874")
