# -*- coding: utf-8 -*-
# @Time    : 2024-03-12 13:42
# @Author  : ZHONG XUE
# @Email   : 569252997@qq.com
# @File    : equipment_management.py
"""
    试验设备管理接口
"""
import time

from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import ranStr, checkAPIResponse, executeJavaScript


class EquipmentManagement(CommonAPI):
    """设备管理"""

    def addTestDeviceType(self, LaboratoryID, ParentTestDeviceTypeId="0",ParentName="功能室",ProjectID=current_env['projectID']):
        """新增试验设备类型"""
        # 请求参数
        data = {
            "TypeName": ranStr(5),
            "Memo": None,
            "OrderNo": 1,
            "ParentTestDeviceTypeId": ParentTestDeviceTypeId,
            "ParentName":ParentName,
            "LaboratoryID": LaboratoryID,
            "LabID": LaboratoryID,
            "ProjectID": ProjectID
        }
        # 发送请求
        resp = self.request(self.APIAddTestDeviceType,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getTestDeviceTypeNodeList(self,LabID,projectId=current_env['projectID']):
        """获取设备类别列表"""
        # 请求参数
        data = {
            "projectId": projectId,
            "LabID": LabID
        }

        # 发送请求
        resp = self.request(self.APIGetTestDeviceTypeNodeList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def updateTestDeviceType(self,LaboratoryID, ID,ParentTestDeviceTypeId="0",ParentName="功能室",ProjectID=current_env['projectID']):
        """编辑试验设备类型"""
        # 请求参数
        data = {
            "TypeName": ranStr(5),
            "Memo": None,
            "OrderNo": 1,
            "ID": ID,
            "LaboratoryID": LaboratoryID,
            "modifyTime": time.strftime('%Y-%m-%d %H-%M-%S'), # "2024-03-12 13:59:34",
            "ProjectId": ProjectID,
            "ParentTestDeviceTypeId": ParentTestDeviceTypeId,
            "ParentName": ParentName,
            "LabID": LaboratoryID
        }

        # 发送请求
        resp = self.request(self.APIUpdateTestDeviceType,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def deleteTestParam(self,id):
        """删除试验设备"""
        # 请求参数
        data = {
            "id":id
        }
        # 发送请求
        resp = self.request(self.APIDeleteTestDeviceType,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    # 试验设备
    def addTestDevice(self, LaboratoryID, TypeID, TypeName, ProjectID=current_env['projectID']):
        """添加试验设备"""
        # 请求参数
        data = {
            "DeviceID": executeJavaScript('getGuid'),
            "TypeID": TypeID,
            "TypeName": TypeName,
            "TestDeviceName": ranStr(5),
            "Code": ranStr(5),
            "Manufacturer": ranStr(5),
            "ReleaseDate": time.strftime('%Y-%m-%d'),
            "SerialNumber": ranStr(5),
            "BuyDate": time.strftime('%Y-%m-%d'),
            "MeasureArea": ranStr(5),
            "PrecisionType": 1,
            "Precision": "0.5",
            "TestCycle": 1,
            "ControlNum": ranStr(5),
            "Status":"1",
            "IsNeedCheck": "false",
            "ImportCode": None,
            "IsBeiJingHYL": "false",
            "Memo": ranStr(5),
            "LaboratoryID": LaboratoryID,
            "LaboratoryIDs": LaboratoryID,
            "Spec": ranStr(5),
            "CompanyAffiliation": ranStr(5),
            "Usage":  ranStr(5),
            "ProjectID": ProjectID
        }

        # 发送请求
        resp = self.request(self.APIAddTestDevice,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getDeviceList(self,LaboratoryId,TypeID,searchKey="",code="",status="正常",page=1,rows=10,ProjectID=current_env['projectID']):
        """获取设备列表"""
        status_dict = {"正常":"1","故障":"0"}
        # 请求参数
        data = {
            "rows": rows,
            "page": page,
            "TypeID": TypeID,
            "LaboratoryId": LaboratoryId,
            "Code": code,
            "SearchKey": searchKey,
            "Status": status_dict[status],
            "ProjectID": ProjectID
        }

        # 发送请求
        resp = self.request(self.APIGetDeviceList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp


equipmentManagementObj = EquipmentManagement()

if __name__ == '__main__':
    res = equipmentManagementObj.addTestDevice(389,2895,'Auto_iuYUL')
    print(res)


