# -*- coding: UTF-8  -*-
# @time     : 2023-09-12 15:31
# @Author   : Zhong Xue
# @File     : laboratory_information_management.py
from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr, ranInt


class LaboratoryInformationManagement(CommonAPI):
    """试验室信息管理"""

    def getLaboratoryInfoByLabID(self, labId, projectID=current_env['projectID']):
        """通过试验室id获取试验室信息"""
        # 请求参数
        data = {
            "labId": labId,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetLaboratoryInfoByLabID,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def updateLaboratoryInfo(self, inData, ProjectID=current_env['projectID']):
        """更新试验室信息"""
        # 请求参数
        data = {
            "ProjectID": ProjectID,
            "PostCode": None
        }
        data.update(inData)

        # 发送请求
        resp = self.request(self.APIUpdateLaboratoryInfo, data)


        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

laboratoryInformationManagementObj = LaboratoryInformationManagement()
if __name__ == '__main__':
    resp = laboratoryInformationManagementObj.getLaboratoryInfoByLabID(269)
    print(resp)
