# -*- coding: utf-8 -*-
# @Time    : 2024-03-11 15:10
# @Author  : ZHONG XUE
# @Email   : 569252997@qq.com
# @File    : personnel_training_registration.py
"""
    人员培训登记页面涉及功能相关接口请求封装
"""
import time

from api.baseApi import CommonAPI, commonAPIObj
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr


class PersonnelTrainingRegistration(CommonAPI):
    """人员培训登记"""

    def getLabPersonTrainList(self,keyWord="",startDate="",endDate="",LabID="",trainType="",page=1,rows=10,ProjectID=current_env['projectID']):
        """获取人员培训列表"""

        # 请求参数
        data = {
            "ProjectID": ProjectID,
            "page": page,
            "rows": rows,
            "keyWord": keyWord,
            "startDate": startDate,
            "endDate": endDate,
            "count": "0",
            "LabID": None if LabID == "" else LabID,
            "trainType": trainType
        }
        # 发送请求
        resp = self.request(self.APIGetLabPersonTrainList,data)


        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def createLabPersonTrain(self,labID,trainPerson,*TrainPersonIdArray,trainTypeName="标准规范",ProjectID=current_env['projectID']):
        """创建人员培训登记"""

        trainType = {"标准规范":"1","行业管理办法":"2","继续教育":"3","专业而配置":"4","其他":"5"}

        # 请求参数
        data = {
            "trainContent": ranStr(5),
            "trainType": trainType[trainTypeName],
            "trainDate": time.strftime('%Y-%m-%d'),
            "trainPlace": ranStr(5),
            "trainUnitOrTeacher": ranStr(5),
            "trainPerson": trainPerson,
            "trainEffect": ranStr(5),
            "TrainPersonIdArray": list(TrainPersonIdArray),
            "memo": ranStr(20),
            "ProjectID": ProjectID,
            "labID":labID
        }
        # 发送请求
        resp = self.request(self.APICreateLabPersonTrain,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def updateLabPersonTrain(self,labID,id,trainPerson,*TrainPersonIdArray,trainTypeName="标准规范",ProjectID=current_env['projectID']):
        """编辑人员培训"""

        trainType = {"标准规范": "1", "行业管理办法": "2", "继续教育": "3", "专业而配置": "4", "其他": "5"}

        # 请求参数
        data = {
            "id": id,
            "trainContent": ranStr(5),
            "trainType": trainType[trainTypeName],
            "trainDate": time.strftime('%Y-%m-%d'),
            "trainPlace": ranStr(5),
            "trainUnitOrTeacher": ranStr(5),
            "trainPerson": trainPerson,
            "trainEffect": ranStr(5),
            "TrainPersonIdArray": list(TrainPersonIdArray),
            "memo": ranStr(20),
            "ProjectID": ProjectID,
            "labID": labID
        }
        # 发送请求
        resp = self.request(self.APIUpdateLabPersonTrain,data)

        # 处理响应
        resp = checkAPIResponse(resp, data)

        # 返回结果
        return resp

    def deleteLabPersonTrain(self,*trainIds):
        """删除人员培训登记"""

        # 请求参数
        data = list(trainIds)

        # 发送请求
        resp = self.request(self.APIDeleteLabPersonTrain,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp


personnelTrainingRegistrationObj = PersonnelTrainingRegistration()
if __name__ == '__main__':
    # res = personnelTrainingRegistrationObj.getLabPersonTrainList()
    # res = personnelTrainingRegistrationObj.updateLabPersonTrain(389,816,"李四,张三","a332d322-39ec-4952-b1a8-6fc022622043", "12406b8c-1a88-4b9a-9e38-dbca3ccc35e3")
    # print(res)
    res = personnelTrainingRegistrationObj.deleteLabPersonTrain(811)
    print(res)