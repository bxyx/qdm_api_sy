# -*- coding: UTF-8  -*-
# @time     : 2023-09-07 14:59
# @Author   : Zhong Xue
# @File     : personnel_information_management.py
import datetime

from api.baseApi import CommonAPI, commonAPIObj
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr, generate_id_number, getPassTime, getDeltaTime, executeJavaScript, \
    ranInt


class PersonnelInformationManagement(CommonAPI):
    """人员信息管理"""

    def addOrUpdatePerson(self, userId, laboratoryID, status='在职', isAdd=True, projectID=current_env['projectID']):
        """新增或编辑人员信息"""
        IDCardNo = generate_id_number()
        Sex = '0' if int(IDCardNo[-2]) % 2 == 0 else '1'
        BirthDate = datetime.datetime(int(IDCardNo[6:10]),int(IDCardNo[10:12]),int(IDCardNo[12:14]),0,0)
        statusDict = {'在职': 1,'离职': 2}
        # 请求参数
        data = {
                 "OpUserId": userId,
                 "IsAdd": isAdd,
                 "Person":{
                       "FullName": ranStr(5),
                       "IDCardNo": IDCardNo,
                       "Sex": Sex,
                       "BirthDate": str(BirthDate),  # "1950-12-16T00:00:00.000Z",
                       "Dept": ranStr(5),
                       "JobTitle": ranStr(5),
                       "Title": ranStr(5),
                       "Hiredate": getPassTime(365).split(' ')[0],
                       "Status": statusDict[status],
                       "TestWorkYear": 5,
                       "Education": "博士研究生",
                       "GraduateSchool": ranStr(5),
                       "Major": ranStr(5),
                       "GraduateDate": getPassTime(700).split(' ')[0],
                       "ContractBeginDate": getPassTime(365).split(' ')[0],
                       "ContractEndDate": getDeltaTime(365).split(' ')[0],
                       "SocialSecurity":"城镇职工社保",
                       "LaboratoryId":laboratoryID,
                       "id": executeJavaScript('getGuid')
                    },
                "ProjectID": projectID}

        # 发送请求
        resp = self.request(self.APIAddOrUpdatePerson,data)

        # 处理响应
        resp = checkAPIResponse(resp, data)

        # 返回结果
        return resp

    def getPersonList(self, laboratoryID, Search='', status='全部', projectID=current_env['projectID']):
        """获取人员信息列表"""
        statusDict = {'在职': 1, '离职': 2, '全部': None}

        # 请求参数
        data = {
            "LabId": laboratoryID,
            "Search": Search,
            "Status": statusDict[status],
            "Pagination": {"rows":10,"page":1,"sidx":"","sord":"","records":0,"total":0},
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIGetPersonList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deletePersonInfo(self, userId, personId, projectID=current_env['projectID']):
        """删除人员信息"""
        # 请求参数
        data = {
                  "OpUserId": userId,
                  "PersonId": personId,
                  "ProjectID": projectID
              }

        # 发送请求
        resp = self.request(self.APIDeletePersonInfo,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getPersonnelDetailById(self, personId, projectID=current_env['projectID']):
        """通过人员id获取人员信息"""
        # 请求参数
        data = {
            "personId": personId,
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIGetPersonnelDetailById,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def addOrUpdatePersonCert(self, userId, personId, certType='试验检测员', IsAdd=True, projectID=current_env['projectID']):
        """新增或编辑人员证书"""
        certTypeDict = {"试验检测员": 1, "试验检测工程师": 2}
        # 请求参数
        data = {
             "OpUserId": userId,
            "IsAdd": IsAdd,
            "PersonCert":{
                "CertNo": ranInt(5),
                "CertType": certTypeDict[certType],
                "CertMajor": "道路工程",
                "IssueDate": getPassTime(365).split(' ')[0],
                "MinLearnHours": 10,
                "ExpirationDate": getDeltaTime(50).split(' ')[0],
                "PersonId": personId,
                "Id": executeJavaScript('getGuid')
            },
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIAddOrUpdatePersonCert,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getPersonCertList(self,personId, projectID=current_env['projectID']):
        """获取人员对应的人员证书列表"""
        # 请求参数
        data = {
            "personId": personId,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetPersonCertList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def addPersonLearnHours(self, certId, projectID=current_env['projectID']):
        """添加证书学时"""
        # 请求参数
        data = {
             "Hours": 1,
             "Memo": ranStr(20),
             "Id": executeJavaScript('getGuid'),
             "CertId": certId,
             "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIAddPersonLearnHours,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getPersonCertHourById(self, Id, projectID=current_env['projectID']):
        """获取证书学时"""
        # 请求参数
        data = {
            "Id": Id,
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIGetPersonCertHourById,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getPersonLearnHourListByCertId(self, certId, projectID=current_env['projectID']):
        """获取证书学时列表"""
        # 请求参数
        data = {
            "certId": certId,
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIGetPersonLearnHourListByCertId,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deleteLearnHours(self, Id, projectID=current_env['projectID']):
        """删除学时登记信息"""
        # 请求参数
        data = {
                "Id": Id,
                "ProjectID": projectID
           }

        # 发送请求
        resp = self.request(self.APIDeleteLearnHours,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deletePersonCert(self, userId, certId, projectID=current_env['projectID']):
        """删除学时证书"""
        # 请求参数
        data = {
             "OpUserId": userId,
             "PersonCertId": certId,
             "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIDeletePersonCert,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def createOrUpdatePersonWorkRecord(self, personId, projectID=current_env['projectID']):
        """新增工作记录"""

        # 请求参数
        data = {
                 "RecordContent":f"<p>{ranStr(20)}<br/></p>",
                 "StartTime": getPassTime(365).split(' ')[0],
                 "EndTime": getPassTime(10).split(' ')[0],
                 "Workplace": ranStr(5),
                 "PersonId": personId,
                 "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APICreateOrUpdatePersonWorkRecord,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getPersonWorkRecordById(self, personId, projectID=current_env['projectID']):
        """通过人员id获取人员工作记录"""
        # 请求参数
        data = {
            "personId": personId,
            "ProjectID": projectID

        }
        # 发送请求
        resp = self.request(self.APIGetPersonWorkRecordById,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deletePersonWorkRecord(self, Id, projectID=current_env['projectID']):
        """删除人员工作记录"""
        # 请求参数
        data = {
                 "Id": Id,
                 "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIDeletePersonWorkRecord,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def addOrUpdatePersonAchievement(self, userId, personId, IsAdd=True, projectID=current_env['projectID']):
        """新增人员工作记录"""
        # 请求参数
        data = {
            "OpUserId": userId,
            "IsAdd": IsAdd,
            "PersonAchievement":{
                "Project": ranStr(5),
                "Post": ranStr(5),
                "BeginDate": getPassTime(365).split(' ')[0],
                "EndDate": getPassTime(10).split(' ')[0],
                "PersonId": personId,
                "Id": executeJavaScript('getGuid')
            },
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIAddOrUpdatePersonAchievement,data)

        # 处理响应
        resp = checkAPIResponse(resp, data)

        # 返回结果
        return resp

    def getPersonAchievement(self, personId, projectID=current_env['projectID']):
        """获取人员业绩信息"""
        # 请求参数
        data = {
            "personId": personId,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetPersonAchievement,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deletePersonAchievement(self, userId, personAchievementId, projectID=current_env['projectID']):
        """删除人员业绩信息"""
        # 请求参数
        data = {
            "OpUserId": userId,
            "PersonAchievementId": personAchievementId,
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIDeletePersonAchievement,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    # 人员附件
    def getPersonAttachmentFolderTree(self, personId, projectID=current_env['projectID']):
        """获取人员附件目录信息"""
        # 请求参数
        data = {
            "personId": personId,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetPersonAttachmentFolderTree,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def addOrUpdatePersonAttachmentFolder(self,userId,personId,parentFolderId,isAdd=True,projectID=current_env['projectID']):
        """新增或编辑附件目录"""
        # 请求参数
        data = {
            "OpUserId": userId,
            "IsAdd": isAdd,
            "PersonId": personId,
            "FolderId": executeJavaScript('getGuid'),
            "FolderName": ranStr(5),
            "ParentFolderId": parentFolderId,
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIAddOrUpdatePersonAttachmentFolder,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def deletePersonAttachmentFolder(self, userId, folderId, projectID=current_env['projectID']):
        """删除附件目录"""
        # 请求参数
        data = {
            "OpUserId": userId,
            "FolderId": folderId,
            "ProjectID": projectID
        }

        # 发送其你去
        resp = self.request(self.APIDeletePersonAttachmentFolder,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    # 人员培训登记
    def createPersonTrain(self, laboratoryID, trainPerson,trainPersonId, trainType="标准规范",projectID=current_env['projectID']):
        """创建人员培训登记"""
        trainTypeDict = {"标准规范":1,"行业管理办法":2,"继续教育":3,"专业培训":4,"其他":5}
        # 请求参数
        data = {
            "trainContent": ranStr(5),  # 培训主题
            "trainType": trainTypeDict[trainType],
            "trainDate": getPassTime(10).split(' ')[0],  #培训日期
            "trainPlace": ranStr(5), # 培训地点
            "trainUnitOrTeacher": ranStr(5),  # 培训地点
            "trainPerson": trainPerson,
            "trainEffect": ranStr(5), # 培训效果
            "TrainPersonIdArray":[trainPersonId],
            "ProjectID": projectID,
            "labID": laboratoryID
        }

        # 发送请求
        resp = self.request(self.APICreatePersonTrain,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getPersonTrainPageList(self,keyWord,projectID=current_env['projectID']):
        """获取人员培训信息列表"""
        # 请求参数
        data = {
            "ProjectID": projectID,
            "page": 1,
            "rows": 20,
            "keyWord": keyWord,
            "startDate": "",
            "endDate": "",
            "count": 0,
            "trainType": ""
        }

        # 发送请求
        resp = self.request(self.APIGetPersonTrainPageList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deletePersonTrain(self,*ID):
        """删除人员培训登记信息"""
        # 请求参数
        data = list(ID)

        # 发送请求
        resp = self.request(self.APIDeletePersonTrain,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    # 人员工作授权
    def getBuildTestProjectTree(self, projectID=current_env['projectID']):
        """获取授权项目目录树"""
        # 请求参数
        data = {"projID": projectID}

        # 发送请求
        resp = self.request(self.APIGetBuildTestProjectTree,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        testItemDict = {}
        for one in resp:
            if(one.get('children')):
                for two in one['children']:
                    testItemDict[two['text']] = two['id']

        # 返回结果
        return testItemDict

    def savePersonItemAuth(self,personId, *ItemIds, projectID=current_env['projectID']):
        """人员工作授权"""
        # 请求参数
        data = {
            "PersonId": personId,
            "ItemIds": list(ItemIds),
            "Memo": ranStr(20),
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APISavePersonItemAuth,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getPersonItemAuthList(self, search, projectID=current_env['projectID']):
        """获取工作授权列表"""
        # 请求参数
        data = {"pagination": {"rows": 10,"page": 1}, "ProjectID": projectID,"seatch": search}

        # 发送请求
        resp = self.request(self.APIGetPersonItemAuthList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deletePersonItemAuth(self, Id, projectID=current_env['projectID']):
        """删除人员工作授权信息"""
        # 请求参数
        data = {
            "Id": Id,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIDeletePersonItemAuth,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp



personnelInformationManagementObj = PersonnelInformationManagement()
if __name__ == '__main__':
    res = commonAPIObj.getLoginUserInfo()
    contractId = res['contractId']
    userId = res['userId']
    laboratoryID = res['laboratoryID']
    # personnelInformationManagementObj.createPersonTrain(laboratoryID,"Auto_tXpG6","12c05afe-b6cf-4d40-d022-e70f9af6a795")
    # resp = personnelInformationManagementObj.getBuildTestProjectTree()
    # print(resp)
