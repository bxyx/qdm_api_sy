# -*- coding: UTF-8  -*-
# @time     : 2023-10-18 14:30
# @Author   : Zhong Xue
# @File     : traverse_point.py
from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import ranStr, executeJavaScript, checkAPIResponse


class TraversePoint(CommonAPI):
    """导线点"""
    def getDictByType(self, type, projectID=current_env['projectID']):
        """通过字典类型获取字典信息"""
        # 请求参数
        data = {
            "type": type,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetDictByType,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def addTestSiteData(self, ContractSectID, GradeId, SiteType, isAdd='true'):
        """新增导线点数据"""
        # 请求参数
        payload1 = {"isAdd": isAdd}
        payload2 = {
            "ContractSectID": ContractSectID,
            "Name": ranStr(5),
            "X":"2",
            "Y":"3",
            "Altitude":"4",
            "GradeId":GradeId,
            "Remark":"5",
            "SiteType":SiteType,
            "ProjectID":281,
            "ID": executeJavaScript('getGuid')
        }
        # 发送请求
        resp = self.request(self.APIAddTestSiteData,payload1,payload2)

        # 处理响应
        resp = checkAPIResponse(resp,payload2)

        # 返回结果
        return resp

    def getTestSiteList(self,contractSectID, searchText, type, projectID=current_env['projectID']):
        """查询导线点信息"""
        # 请求参数
        payload1 = {
            "projetID": projectID,
            "type": type,
            "name": searchText,
            "gradeId": "",
            "contractSectID": contractSectID
        }
        payload2 = {
            "rows":10,
            "page":1,
            "sidx":"",
            "sord":"",
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetTestSiteList,payload1,payload2)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deleteTestSite(self,id, projectID=current_env['projectID']):
        """删除导线点数据"""
        # 请求参数
        payload1 = {'id': id}
        payload2 = {"ProjectID":projectID}

        # 发送请求
        resp = self.request(self.APIDeleteTestSite,payload1,payload2)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

traversePointObj = TraversePoint()
if __name__ == '__main__':
    res = traversePointObj.getTestSiteList(759,"Auto_FZs13")
    print(res)

