# -*- coding: UTF-8  -*-
# @time     : 2023-10-18 9:12
# @Author   : Zhong Xue
# @File     : experiment_query.py
import os

import xlrd

from api.baseApi import CommonAPI
from common.mySetting import current_env, externalFilePath
from common.public import checkAPIResponse, getPassTime, getDeltaTime


class ExperimentQuery(CommonAPI):
    """试验查询统计"""

    def getTestList(self, Lab, contract, taskId, keyword="", projectId=current_env['projectID']):
        """查询试验信息"""
        # 请求参数
        data = {
            "ProjectID": projectId,
            "keyword": keyword,
            "taskId": taskId,
            "startdate": "",
            "enddate": "",
            "bdtype": "",
            "contractStr": contract,
            "testproject": "",
            "LabStr": Lab,
            "UnitType": "0",
            "isQualified": "",
            "QualificationType": "1",
            "taskState": "",
            "createUserId": "",
            "u": "autosgsysfzr1",
            "isCurUser": "true",
            "page": 1,
            "rows": 20
        }
        # 发送请求
        resp = self.request(self.APIGetTestList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def getLogs(self, taskID):
        """查看日志"""
        # 请求参数
        data = {"taskID": taskID}

        # 发送请求
        resp = self.request(self.APIGetLogs,data )

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def exportTestTask(self,Lab, contract, userName, projectId=current_env['projectID']):
        """导出试验数据"""
        # 请求参数
        data = {
            "ProjectID": projectId,
            "keyword":"",
            "taskId":"",
            "bdtype":"",
            "contractStr": contract,
            "startdate": getPassTime(10),
            "enddate": getDeltaTime(10),
            "u": userName,
            "LabStr": Lab,
            "UnitType": "0",
            "isCurUser": "true"
        }

        # 发送请求
        resp = self.request(self.APIExportTestTask,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

experimentQueryObj = ExperimentQuery()
if __name__ == '__main__':
    # res = experimentQueryObj.exportTestTask(269,759,'autosgsysfzr1')
    # with open(os.path.join(externalFilePath, '试验检测.xls'), 'wb') as f:
    #     f.write(res)

    # 断言
    wb = xlrd.open_workbook(os.path.join(externalFilePath, '试验检测.xls'))
    resp = wb.sheet_by_name("试验检测记录")
    print(resp.row_values(0))
    print(resp.nrows>1)

