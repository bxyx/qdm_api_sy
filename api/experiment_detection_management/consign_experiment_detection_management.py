# -*- coding: UTF-8  -*-
# @time     : 2023-10-16 10:21
# @Author   : Zhong Xue
# @File     : consign_experiment_detection_management.py
import time

from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr, getDeltaTime, executeJavaScript, ranInt


class ConsignExperimentDetectionManagement(CommonAPI):
    """外委检测管理"""

    def getConsignTestAllTables(self):
        """获取外委试验统计标签"""
        # 请求参数
        # 发送请求
        resp = self.request(self.APIGetConsignTestAllTables)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def getConsignTestResultType(self):
        """获取外委试验结果类型"""
        # 请求参数
        # 发送请求
        resp = self.request(self.APIGetConsignTestResultType)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def createConsignTestCls(self,TestResultType,ParamResultType,LableName,isThirdTest='false',DataType=0,projectId=current_env['projectID']):
        """创建外委试验类型"""
        # 请求参数
        data = {
            "Name": ranStr(5),
            "Memo": ranStr(5),
            "TestResultType": TestResultType,
            "TestResultTypeName": ranStr(5),
            "ParamResultType": ParamResultType,
            "ParamResultTypeName": ranStr(5),
            "IsMaterial": 'false',
            "DataType": DataType,
            "projectId": projectId,
            "ParentLevelCode": None,
            "isThirdTest": isThirdTest,
            "LableNames": LableName
        }

        # 发送请求
        resp = self.request(self.APICreateConsignTestCls,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        return resp

    def getConsignTestTree(self,isThirdTest='false',DataType=0, projectId=current_env['projectID']):
        """获取外委试验类型树"""
        # 请求参数
        data = {
            "projectId": projectId,
            "isThirdTest": isThirdTest,
            "dataType": DataType
        }
        # 发送请求
        resp = self.request(self.APIGetConsignTestTree,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def deleteConsignTest(self,ID):
        """删除外委试验类型"""
        # 请求参数
        data = [ID]

        # 发送请求
        resp = self.request(self.APIDeleteConsignTest,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def createConsignTest(self, UserUnitID, ContractSectID, ConsignTestClsID, TestSampleMasterID, SampleName,SampleCode,ProjectId=current_env['projectID']):
        """创建外委试验"""
        # 请求参数
        data = {
            "ContractSectID": ContractSectID,  # 合同段
            "TestSampleMasterID": TestSampleMasterID,
            "SampleName": SampleName,  # 样品名称
            "ConsignNo": ranStr(5),  # 委托编号
            "ReportNo": ranStr(5), # 报告编号
            "TestEngineer": ranStr(5), # 监理试验工程师
            "InspectionNum": ranStr(5), #报验编号
            "ConsignUnit": ranStr(5),  # 委托单位
            "AcceptTestUnit": ranStr(5),  # 检测机构
            "ConsignUnitAbility": ranStr(5),  # 检测机构资质证书编号
            "TakeSamplingDate": time.strftime('%Y-%m-%d'),
            "SendSampleDate": time.strftime('%Y-%m-%d'),
            "TestDate": time.strftime('%Y-%m-%d'),
            "TestDateEnd": getDeltaTime(3),
            "ReportDate": getDeltaTime(3),
            "TestUserFriendlyName": ranStr(5),  # 试验操作人
            "ProjectPart": ranStr(5), # 工程部位
            "BatchNum": ranStr(5),  # 批号/编号
            "Quantity": "1",
            "QuantityUnitName":"kg",
            "SampleCode": SampleCode,  # 样品编号
            "GuiGeXingHao": ranStr(5),  # 规格型号
            "ShengChanCJ": ranStr(5),  # 生产厂家
            "ChanDi": ranStr(5), # 产地
            "SamplingPlace": ranStr(5),  # 取样地点
            "SampleNum":"1",
            "SampleUnitName":"kg",
            "WitnessPerson": ranStr(5),  # 见证人
            "SendSamplePerson": ranStr(5),  # 送样人
            "PeriodNum":"1",
            "IsQualified":"1",
            "Result":"",
            "TestStandard": ranStr(5),  # 检测依据
            "AssessStandard": ranStr(5),  # 评定标准
            "TestConclusion": ranStr(5),  # 检测结论
            "Memo": ranStr(5),
            "UserUnitID":UserUnitID,
            "ConsignTestClsID":ConsignTestClsID,
            "ConsignDocFileName":executeJavaScript('getGuid'),
            "RecordDocFileName1":executeJavaScript('getGuid'),
            "ReportDocFileName":executeJavaScript('getGuid'),
            "OtherFileName":executeJavaScript('getGuid'),
            "HasConsignDoc":'false',
            "HasReportDoc":'false',
            "HasRecordDoc":'false',
            "ProjectId":ProjectId,
            "QuantityUnit":"",
            "SampleUnit":""
        }
        # 发送请求
        resp = self.request(self.APICreateConsignTest,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        return resp

    def getConsignTestPageList(self, userUnitID, contractSectID, keyWord, consignTestClsID, isThirdTest='false',DataType=0,ProjectId=current_env['projectID']):
        """获取委托试验列表"""
        # 请求参数
        data = {
            "ProjectID": ProjectId,
            "contractSectID": contractSectID,
            "keyWord": keyWord,
            "reportStartDate": "",
            "reportEndDate": "",
            "consignTestClsID": consignTestClsID,
            "isThirdTest": isThirdTest,
            "isShowOtherLab": "true",
            "userUnitID": userUnitID,
            "ReportType": "",
            "DataType": DataType,
            "Status": 0,
            "page": 1,
            "rows": 20
        }

        # 发送请求
        resp = self.request(self.APIGetConsignTestPageList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def getConsignTestById(self,ConsignTestID):
        """获取外委试验信息"""
        # 请求参数
        data = {"ConsignTestID": ConsignTestID}

        # 发送请求
        resp = self.request(self.APIGetConsignTestById,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def deleteAddedConsignTest(self,*args):
        """删除新增的外委试验"""
        # 请求参数
        data = list(args)
        # 发送请求
        resp = self.request(self.APIDeleteAddedConsignTest,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def createConsignTestDetail(self,ConsignTestID,ConsignTestClsID):
        """新增检测参数"""
        # 请求参数
        data = {
            "TestParam":ranStr(5),
            "TestGroup":ranInt(1),
            "StandardValue":ranInt(1),
            "Result":"",
            "Memo":ranStr(5),
            "ConsignTestID":ConsignTestID,
            "ConsignTestClsID":ConsignTestClsID,
            "TestValue":""
        }
        # 发送请求
        resp = self.request(self.APICreateConsignTestDetail,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        return resp

    def getConsignTestDetailList(self, ConsignTestID, ProjectId=current_env['projectID']):
        """获取检测参数详情"""
        # 请求参数
        data = {
            "ProjectID": ProjectId,
            "ConsignTestID": ConsignTestID
        }

        # 发送请求
        resp = self.request(self.APIGetConsignTestDetailList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def deleteConsignTestDetail(self,*args):
        """删除检测参数"""
        # 请求参数
        data = list(args)

        # 发送请求
        resp = self.request(self.APIDeleteConsignTestDetail,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp


consignExperimentDetectionManagementObj = ConsignExperimentDetectionManagement()
if __name__ == '__main__':
    res = consignExperimentDetectionManagementObj.getConsignTestDetailList('2676')
    print(res)

