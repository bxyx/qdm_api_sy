# -*- coding: UTF-8  -*-
# @time     : 2023-10-08 15:00
# @Author   : Zhong Xue
# @File     : experiment_detection.py
import json
import os
import time

from api.baseApi import CommonAPI, APILogIn
from common.mySetting import current_env, externalFilePath
from common.public import checkAPIResponse, ranStr


class ExperimentDetection(CommonAPI):
    """试验检测"""

    def getBuildTestProjectTree(self, projectID=current_env['projectID']):
        """获取测试项目目录树"""
        # 请求参数
        data = {
                    "projID": projectID,
                    "isCotainTestSumary": "false",
                    "isConsignTest": "false",
                    "isMaterialInput": "false",
                    "qualificationType": 1
                }

        # 发送请求
        resp = self.request(self.APIGetBuildTestProjectTree, data)

        # 处理响应
        resp = checkAPIResponse(resp)

        testItemDict = {}
        for one in resp:
            if one.get('children'):
                for two in one['children']:
                    tmp = [two['id'],json.loads(two['attributes']['SampleTypeJson'])]
                    testItemDict[two['text']] = tmp

        # 返回结果
        return testItemDict

    def getAllSampleMaster(self, contractSectID, materialTypeID, sampleCode, projectID=current_env['projectID']):
        """获取所有样品信息"""
        # 请求参数
        data = {
            "ProjectID": projectID,
            "contractSectID": contractSectID,
            "materialTypeID": materialTypeID,
            "IsRelationTestTask": "false",
            "Key": sampleCode,
            "page": 1,
            "rows": 20
        }

        # 发送请求
        resp = self.request(self.APIGetAllSampleMaster,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def relationTestSample(self, TestSampleMasterId, itemId, projectID=current_env['projectID']):
        """关联试验和样品"""
        # 请求参数
        data = {
            "TestSampleMasterId": TestSampleMasterId,
            "itemId": itemId,
            "projectId": projectID,
            "TaskId": 0
        }
        # 发送请求
        resp = self.request(self.APIRelationTestSample,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def createTest(self,ssoName,ContractSectID,ItemID, sampleId='',sampleCode='',QualificationType='',isRelateSample=True):
        """新增试验"""
        # 请求参数
        if isRelateSample:
            data = {
                "ItemID":ItemID,
                "ssoName":ssoName,
                "ContractSectID":ContractSectID,
                "sampleId":sampleId,
                "sampleCode":sampleCode,
                "testType":"自检",
                "QualificationType":QualificationType
            }
        else:
            data = {
                "ItemID": ItemID,
                "ssoName": ssoName,
                "ContractSectID": ContractSectID,
                "QualificationType": QualificationType
            }

        # 发送请求
        resp = self.request(self.APICreateTest,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def updateTaskID(self,TaskId,Id):
        """更新任务ID"""
        # 请求参数
        data = {"TaskId":TaskId,"Id":Id}
        # 发送请求
        resp = self.request(self.APIUpdateTaskID,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def getTestTaskListV2(self, ssoName, ContractSectID, ItemId, keyword,tabIndex=0,taskState="1,3,8",projectID=current_env['projectID']):
        """获取试验信息列表"""
        # 请求参数
        searchDict = {
                        "ProjectID": projectID,
                        "ItemId":ItemId,
                        "keyword":keyword,
                        "startdate":time.strftime("%Y-%m-%d"),
                        "enddate":"",
                        "ContractSectID":ContractSectID,
                        "bdtype":"Contract",
                        "tabIndex":tabIndex,
                        "DataType":None,
                        "user":ssoName,
                        "labId":None,
                        "onlyShowSampleChanged":0,
                        "taskState":taskState,  #
                        "IsDeleted":0,
                        "QualificationType":1
                  }
        data = {
                  "queryJson": json.dumps(searchDict),
                  "page":1,
                  "rows":20,
                  "sort":"TaskID",
                  "order":"desc"
        }

        # 发送请求
        resp = self.request(self.APIGetTestTaskListV2,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def execCancelWorkFlow(self,foreignKey,foreignTable):
        """撤销试验"""
        # 试验参数
        data = [{"foreignTable":foreignTable,"foreignKey":foreignKey}]

        # 发送请求
        resp = self.request(self.APIExecCancelWorkFlow,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def copyTestTask(self,userId,ssoName,srcTaskID):
        """复制试验任务"""
        # 请求参数
        data = {
            "srcTaskID": srcTaskID,
            "ssoName": ssoName,
            "userId": userId,
            "sampleId": None
        }

        # 发送请求
        resp = self.request(self.APICopyTestTask,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def deleteTestTask(self,ssoName, *args):
        """删除试验任务"""
        # 请求参数
        data = {"TaskIDs":list(args),"ssoName":ssoName}

        # 发送请求
        resp = self.request(self.APIDeleteTestTask,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def commitWorkFlow(self,inData):
        """提交试验"""
        # 请求参数
        # 发送请求
        resp = self.request(self.APICommitWorkFlow,inData)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def queryTaskProcess(self,taskID):
        """获取试验任务进度"""
        # 请求参数
        data = {"taskID":taskID,"dataType":"1"}

        # 发送请求
        resp = self.request(self.APIQueryTaskProcess,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getSignTemplates(self):
        """获取签字模版"""
        # 发送请求
        resp = self.request(self.APIGetSignTemplates)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getSignUserInfoById(self,taskID,projectId=current_env['projectID']):
        """获取签字人信息"""
        # 请求参数
        data1 = {
            "projectId": projectId,
            "recordSignUserTemplateMstId": None,
            "isSubitem": "true"
        }
        data2 = [{"taskID":taskID,"dataType":1,"foreignTable":"TestTask"}]

        # 发送请求
        resp = self.request(self.APIGetSignUserInfoById,data1,data2)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def printSetUp(self,userName,mark,report):
        """打印设置"""
        obj = {"userName":userName,"mark":mark,"report":report}
        # 请求参数
        data = {"paramObj": str(obj)}
        # 发送请求
        resp = self.request(self.APIPrintSetUp,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getLogs(self, taskID):
        """获取试验任务日志信息"""
        # 请求参数
        data = {'taskID': taskID}

        # 发送请求
        resp = self.request(self.APIGetLogs,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getTestUsers(self,userId,ProjectID=current_env['projectID']):
        """获取试验任务转交人员"""
        # 请求参数
        data = {
            "userId": userId,
            "ProjectID": ProjectID,
        }
        # 发送请求
        resp = self.request(self.APIGetTestUsers,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        handoverUserDict = dict()
        for one in resp['data']:
            handoverUserDict[one['FriendlyName']] = one['UserId']


        # 返回结果
        return handoverUserDict

    def taskHandover(self, UserId, HandedUserId, TaskGuid):
        """试验任务转交"""
        # 请求参数
        data = {
                "TaskGuid":[TaskGuid],
                "UserIds":[HandedUserId],
                "HandedUserId": UserId,
                "HandedOpinion": ranStr(5)
        }
        # 发送请求
        resp = self.request(self.APITaskHandover,data)


        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp


# -------------进一步封装以上方法------------------

    def commitMaterialInputInfo(self,taskID, isAuditOrSign):
        """试验提交"""
        resp = self.getSignUserInfoById(taskID)
        signUserInfo = resp['data']
        signUserInfo.pop('Name')
        if isAuditOrSign:
            for one in signUserInfo['SignItemList']:
                friendly_name_list = [o['FriendlyName'] for o in one['AllUserChildren']]
                if current_env['execUser'] in friendly_name_list:
                    idx = friendly_name_list.index(current_env['execUser'])
                    one['AllUserChildren'][idx]['IsChecked'] = 'true'
                    one['UserChildren'] = one['AllUserChildren']
                    one['selectUser'] = one['AllUserChildren'][idx]['UserId']
                else:
                    one['AllUserChildren'][0]['IsChecked'] = 'true'
                    one['UserChildren'] = one['AllUserChildren']
                    one['selectUser'] = one['AllUserChildren'][0]['UserId']
        else:
            signUserInfo['SignItemList'] = []

        # 提交签字或审核
        self.commitWorkFlow(signUserInfo)




experimentDetectionObj = ExperimentDetection()
if __name__ == '__main__':
    # resp = experimentDetectionObj.getTestTaskListV2("autosggcbgcs", "721", "", "429025",1,'2,4,5,7,9')
    # print(resp)
    resp = experimentDetectionObj.getBuildTestProjectTree()
    testItemId = resp.get(current_env['materialType'])[0]

    print(testItemId)