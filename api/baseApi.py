# -*- coding: UTF-8  -*-
# @time     : 2023-03-07 10:17
# @Author   : Zhong Xue
# @File     : baseApi.py


# BaseAPI--公共的post,get方法
import base64
import json
import os

import requests

from common.log import log
from common.mySetting import apiYmlPath, current_env, externalFilePath
from common.public import checkAPIResponse
from common.readYml import readYml


def APILogIn(userName=current_env['execUser'],password=current_env['execPsw']):
    """登录系统,获取token"""
    # 1- url--- 协议+ip+port+路径
    url = f'{current_env["host"]}/api/user/Authenticate'

    # 2- 请求体body
    payload = {
        "userNameOrEmailAddress": userName,
        "AuthCode":"",
        "rememberMe":"false",
        "password2": base64.b64encode(str(password).encode()).decode()    # base64编码
    }
    # 3- 发送请求
    resp = requests.post(url, json=payload)
    log.info(f'用户{userName}登录系统->{resp.json()["message"]}')
    assert resp.json()['message'] == "登录成功"
    return resp.json()['result']['jwtUser']['token']

token = APILogIn()


class BaseAPI:
    def __init__(self,token=token):
        self.token = token
        for cls in self.__class__.mro()[:-2]:
            #拿到接口配置
            conf = readYml(apiYmlPath)[cls.__name__]
            for k,v in conf.items():
                setattr(self,k,v)  # 对象,属性名,属性值

    def request(self, api, payload=None, payload1=None, host=current_env["host"]):
        method = api['method']
        format = api['format']

        # 请求头
        header = {'Authorization': self.token}

        # 请求体
        if method == 'post':
            if format == 'json':
                return requests.post(f'{host}{api["path"]}',json=payload, headers=header)
            elif format == 'data':
                return requests.post(f'{host}{api["path"]}', data=payload, headers=header)
            elif format == 'json&data':
                return requests.post(f'{host}{api["path"]}', params=payload, json=payload1, headers=header)
            elif format == 'params':
                return requests.post(f'{host}{api["path"]}', params=payload, headers=header)
            elif format == 'params&file':
                return requests.post(f'{host}{api["path"]}', params=payload, files=payload1,headers=header)

        elif method == 'get':
            if format == 'json':
                return requests.get(f'{host}{api["path"]}',json=payload, headers=header)
            elif format == 'params':
                return requests.get(f'{host}{api["path"]}', params=payload, headers=header)


class CommonAPI(BaseAPI):
    """系统公共api"""

    def userAuthenticate(self,userName,password):
        """登录系统"""

        # 请求参数
        data = {
                "userNameOrEmailAddress": userName,
                "AuthCode":"",
                "rememberMe":"false",
                "password2": base64.b64encode(str(password).encode()).decode()    # base64加密
            }

        # 发送请求
        resp = self.request(self.APIUserAuthenticate,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getLoginUserInfo(self):
        """获取当前登录用户信息"""

        # 请求数据
        data = {}

        # 发送请求
        resp = self.request(self.APIGetLoginUserInfo,data)

        # # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果(返回用户相关字典信息)
        userInfoDict = dict()
        userInfoDict['userId'] = resp['result']['userInfo']['userId']
        userInfoDict['laboratoryID'] = resp['result']['userInfo']['laboratoryID']
        userInfoDict['laboratoryName'] = resp['result']['userInfo']['laboratoryName']
        userInfoDict['ssoName'] = resp['result']['userInfo']['ssoName']
        userInfoDict['userName'] = resp['result']['userInfo']['userName']
        userInfoDict['roleTypeId'] = resp['result']['userInfo']['roleTypeId']
        userInfoDict['token'] = resp['result']['jwtUser']['token']

        for one in resp['result']['userInfo']['contractList']:
            if one['name'] == current_env['contractSection']:
                userInfoDict['contractId'] = one['id']
                userInfoDict['contractName'] = one['name']
        return userInfoDict

    def getMenuTree(self,userId,ProjectID=current_env['projectID']):
        """获取用户菜单"""
        # 请求参数
        data = {
            "userId": userId,
            "SysType": "",
            "ProjectID": ProjectID
        }

        # 发送请求
        resp = self.request(self.APIGetMenuTree,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def uploadAccessory(self, foreignKey, foreignTable, accessoryType, fileName='personnelCert.jpg',foreignKey2=None, ):
        """附件上传"""
        data = {
            "foreignKey": foreignKey,
            "foreignTable": foreignTable,
            "accessoryType": accessoryType,
        }

        if foreignKey2 is not None:
            data['foreignKey2'] = foreignKey2

        file = {'file': (fileName, open(os.path.join(externalFilePath, fileName), 'rb'))}

        # 发送请求
        resp = self.request(self.APIUploadAccessory, data, file)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getAccessory(self, foreignKey, foreignTable, accessoryType, projectID=current_env['projectID']):
        """获取附件信息"""
        # 请求参数
        data = {
            "foreignKey": foreignKey,
            "foreignTable": foreignTable,
            "accessoryType": accessoryType,
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIGetAccessory,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getProjectParam(self, mode="ParamCode",projectId=current_env['projectID']):
        """
            获取系统控制参数
            mode: ParamName or ParamCode
        """

        # 请求参数
        data = {"projectId": projectId}

        # 发送请求
        resp = self.request(self.APIGetProjectParam, data)

        # 处理响应
        resp = checkAPIResponse(resp)
        paramsDict = {}
        for one in resp['data']:
            paramsDict[one[mode]] = one['ParamValue']

        return paramsDict


commonAPIObj = CommonAPI()

if __name__ == '__main__':
    token = APILogIn()


