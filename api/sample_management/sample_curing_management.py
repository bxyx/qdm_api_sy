# -*- coding: UTF-8  -*-
# @time     : 2023-10-07 10:47
# @Author   : Zhong Xue
# @File     : sample_curing_management.py
import time

from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr


class SampleCuringManagement(CommonAPI):
    """样品养护登记"""

    def getTestSamplesForMaintenance(self, ContractSectId, FilterText, ProjectID=current_env['projectID']):
        """获取样品信息"""
        # 请求参数
        data = {
            "page": 1,
            "rows": 10,
            "ContractSectId": ContractSectId,
            "FilterText": FilterText,
            "ProjectID": ProjectID
        }

        # 发送请求
        resp = self.request(self.APIGetTestSamplesForMaintenance,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def sampleCreateMaintenance(self, UserId, ContractSectId, SampleCode, SampleId, SampleName, ProjectID=current_env['projectID']):
        """创建样品养护信息"""
        # 请求参数
        data = {
            "Id": None,
            "ContractSectId": ContractSectId,
            "SampleCode": SampleCode,
            "SampleName": SampleName,
            "Groups": 1,
            "EachGroupCount": 1,
            "Usage": ranStr(5),
            "SamplingDate": time.strftime('%Y-%m-%d'),
            "SamplingAddress": None,
            "TestPieceSize": "1",
            "Condition": "0",
            "Age":"1",
            "EntryDate": time.strftime('%Y-%m-%d'),
            "IntensityGrade": "1",
            "MoldingDate": time.strftime('%Y-%m-%d'),
            "StorageLocation": None,
            "StorageUser": UserId,
            "SampleId": SampleId,
            "ProjectID": ProjectID
        }

        # 发送请求
        resp = self.request(self.APISampleCreateMaintenance,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getSampleMaintenanceList(self, ContractSectId, FilterText, ProjectID=current_env['projectID']):
        """获取样品养护列表"""
        # 请求参数
        data = {
            "rows": 10,
            "page": 1,
            "ContractSectId": ContractSectId,
            "ScheduledDate": "",
            "Condition": 0,
            "FilterText": FilterText,
            "ProjectID": ProjectID
        }

        # 发送请求
        resp = self.request(self.APIGetSampleMaintenanceList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def sampleMaintenanceDelivery(self, ID):
        """养护出库"""
        # 请求参数
        data = [ID]

        # 发送请求
        resp = self.request(self.APISampleMaintenanceDelivery,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def sampleMaintenanceDelete(self, ID, ProjectID=current_env['projectID']):
        """样品养护删除"""
        # 请求参数
        data = {
                "id": ID,
                "ProjectID": ProjectID
        }

        # 发送请求
        resp = self.request(self.APISampleMaintenanceDelete,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp



sampleCuringManagementObj = SampleCuringManagement()
if __name__ == '__main__':
    resp = sampleCuringManagementObj.getTestSamplesForMaintenance(759,'YP-[2023]349')
    print(resp)



