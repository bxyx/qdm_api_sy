# -*- coding: UTF-8  -*-
# @time     : 2023-09-19 10:26
# @Author   : Zhong Xue
# @File     : sample_retention_management.py
import time

import xlrd

from api.baseApi import CommonAPI
from common.mySetting import current_env, externalFilePath
from common.public import checkAPIResponse, getDeltaTime, ranStr


class SampleRetentionManagement(CommonAPI):
    """样品留样登记"""

    def addSampleRetention(self,userId,contractId,testSampleMasterId,sampleCode,sampleName,retentionType="测前留样",projectID=current_env['projectID']):
        """新增样品留样"""
        retentionTypeDict = {"测前留样": 1, "测后留样": 2}
        # 请求参数
        data = {
            "TestSampleMasterId": testSampleMasterId,
            "SampleCode": sampleCode,
            "SampleName": sampleName,
            "Type":retentionTypeDict[retentionType],
            "Count": 1,
            "RetentionUnit": "kg",
            "Date": time.strftime('%Y-%m-%d'),
            "Period": 10,
            "ExpiredDate": getDeltaTime(10).split(' ')[0],
            "StorePlace":ranStr(5),
            "Reason":None,
            "ContractSectId":contractId,
            "OpUserId": userId,
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIAddSampleRetention,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getSampleRetentionPageList(self, userId, contractId, searchText, projectID=current_env['projectID']):
        """获取样品留样列表"""
        # 请求参数
        data = {
            "Search": searchText,
            "RetentionDateBegin": None,
            "RetentionDateEnd": None,
            "ExprieDateBegin": None,
            "ExprieDateEnd": None,
            "ContractSectIds":[contractId],
            "LabIds": None,
            "Status": None,
            "Pagination":{"page":1,"rows":10,"sidx":"","sord":""},
            "OpUserId": userId,
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APIGetSampleRetentionPageList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def sampleRetentionDelay(self, userId, userName, *retentionIds, projectID=current_env['projectID']):
        """样品留样处理-延后留样"""
        # 请求参数
        data = {
            "Type":1,
            "UserFriendlyName": userName,
            "HandleDate": time.strftime('%Y-%m-%d'),
            "ExpiredDate": None,
            "Period":10,
            "TestSampleMasterRetentionIds": list(retentionIds),
            "OpUserId":userId,
            "Date": time.strftime('%Y-%m-%d'),
            "ProjectID": projectID
        }

        # 发送请求
        resp = self.request(self.APISampleRetentionDelay,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def sampleRetentionAbandonV2(self,userId, userName, *retentionIds, projectID=current_env['projectID']):
        """样品留样处理-留样废弃"""
        # 请求参数
        data = {
            "Type":2,
            "UserFriendlyName": userName,
            "HandleDate": time.strftime('%Y-%m-%d'),
            "ExpiredDate": None,
            "Period": None,
            "TestSampleMasterRetentionIds": list(retentionIds),
            "OpUserId": userId,
            "Date": time.strftime('%Y-%m-%d'),
            "Reason": ranStr(5),
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APISampleRetentionAbandonV2,data)

        # 处理响应
        resp = checkAPIResponse(resp, data)

        # 返回结果
        return resp

    def getSampleRetentionListToExcel(self, projectID=current_env['projectID']):
        """导出留样处理信息"""
        # 请求参数
        data = {
            "Pagination": {"page":1,"rows":10,"sidx":"","sord":""},
            "ProjectID": projectID
        }
        # 发送请求
        resp = self.request(self.APIGetSampleRetentionListToExcel,data)

        # 处理响应
        resp = checkAPIResponse(resp, data)

        # 返回结果
        return resp

sampleRetentionManagementObj = SampleRetentionManagement()
if __name__ == '__main__':
    # resp = sampleRetentionManagementObj.getSampleRetentionListToExcel()
    # print(resp)
    #
    # with open('样品留样.xls','wb+') as f:
    #     f.write(resp)

    # with open('%s\%s' % (externalFilePath, '样品留样.xls'), 'rb') as f:
    #     res = f.readlines()
    #     print(res)
    pass






