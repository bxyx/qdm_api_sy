# -*- coding: UTF-8  -*-
# @time     : 2023-09-18 9:03
# @Author   : Zhong Xue
# @File     : sample_master_management.py
import time

import pytest

from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr


class SampleMasterManagement(CommonAPI):
    """材料取样登记"""

    def getMaterialTypeProjectTree(self,projectID=current_env['projectID']):
        """获取材料类型树"""
        # 请求参数
        data = {"projID":projectID}

        # 发送请求
        resp = self.request(self.APIGetMaterialTypeProjectTree,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getSampleMasterNumber(self, contractId, testItemId):
        """材料取样自动编号"""
        # 请求参数
        data = {
            "ContractSectID": contractId,
            "TestItemId": testItemId
        }

        # 发送请求
        resp = self.request(self.APIGetSampleMasterNumber,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getWitnessList(self,projectID=current_env['projectID']):
        """获取见证人列表"""
        # 请求参数
        data = {"projectid": projectID}

        # 发送请求
        resp = self.request(self.APIGetWitnessList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getDefaultWitnessUser(self,materialTypeID, projectID=current_env['projectID']):
        """获取默认见证人信息"""
        # 请求参数
        data = {
            "projectId": projectID,
            "materialTypeID": materialTypeID
        }
        # 发送请求
        resp = self.request(self.APIGetDefaultWitnessUser,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def createSampleMaster(self, userId, laboratoryID, contractId, testItemId, materialTypeID,
                           materialInputID, sampleName, sampleCode, specification, uom,
                           projectPart,witnessCompany,witnessPerson,witnessUserId,sampleAdr,
                           productCompany,dateOfProduction,outputCode,inputDate,facadeQuality,
                           memo,isDirectSampling=False):
        """进场材料取样"""
        # 请求参数
        data = {
            "ContractSectID": contractId,
            "ConsignCode": "",
            "SampleName": sampleName,
            "SampleCode": sampleCode,
            "Specification": specification,
            "Count": "1",
            "Uom": uom,  #
            "RegDate": time.strftime('%Y-%m-%d'),
            "ProjectPart": projectPart,
            "ShowMaterialCount": "1",
            "ShowMaterialUnit": uom,
            "witnessCompany": witnessCompany,
            "witnessPerson": witnessPerson,
            "WitnessUserId": witnessUserId,
            "RetentionPerson": current_env['execUser'],
            "SampleAdr": sampleAdr,
            "ProductCompany": productCompany,
            "DateOfProduction": dateOfProduction,
            "OutputCode": outputCode,
            "InputDate": inputDate,
            "FacadeQuality": facadeQuality,
            "Memo": memo,
            "IsConsign": False,
            "IsAddMaterialInput": False,
            "IsSampleCodeAutoCreate": True,
            "WitnessOrgs": witnessCompany,
            "sampleMaintenance": [],
            "BindTags": [],
            "isCreateTestType": 0,
            "isCreateTest": False,
            "TestItemId": testItemId,
            "PurposeSettingId": None,
            "MaterialInputID": materialInputID,  #
            "UserLabID": laboratoryID,
            "UserUnit": "1",
            "UserID": userId,
            "MaterialTypeID": materialTypeID,
            "IsConfirm": False,
            "ContractPartitions": [],
            "CreateTestSampleCodeItemId": testItemId
        }
        if isDirectSampling:
            data.pop('Uom')
            data.pop('MaterialInputID')

        # 发送请求
        resp = self.request(self.APICreateSampleMaster,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getSampleMasterList(self,laboratoryID,contractId,materialTypeID,SearchKey,projectID=current_env['projectID']):
        """获取材料取样列表"""
        # 请求参数
        data = {
            "page": 1,
            "rows": 20,
            "contractSectID": contractId,
            "userLabID": laboratoryID,
            "ProjectID": projectID,
            "materialTypeID": materialTypeID,
            "StartRegDate": "",
            "EndRegDate": "",
            "Key": SearchKey
        }
        # 发送请求
        resp = self.request(self.APIGetSampleMasterList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def copySampleMaster(self, userId, laboratoryID, Id):
        """复制材料取样登记"""
        # 请求参数
        data = [{
             "Id": Id,
             "UserId": userId,
             "LabId": laboratoryID
        }]

        # 发送请求
        resp = self.request(self.APICopySampleMaster,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deleteSampleMaster(self,*args):
        """删除材料进场登记"""
        # 请求参数
        data = list(args)

        # 发送请求
        resp = self.request(self.APIDeleteSampleMaster,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp




sampleMasterManagementObj = SampleMasterManagement()
if __name__ == '__main__':
    resp = sampleMasterManagementObj.getSampleMasterList('269','759','1','YP20234026')
    print(resp)
