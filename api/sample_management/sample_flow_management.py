# -*- coding: UTF-8  -*-
# @time     : 2023-10-07 9:37
# @Author   : Zhong Xue
# @File     : sample_flow_management.py
import time

from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr


class SampleFlowManagement(CommonAPI):
    """样品流转管理"""

    def getDaiRuKuPagingList(self,UserId,ContractSectId,LabId,Search,ProjectID=current_env['projectID']):
        """获取待入库样品列表"""
        # 请求参数
        data = {
            "ProjectID":ProjectID,
            "OpUserId":UserId,
            "MaterialTypeID":None,
            "Search":Search,
            "ContractSectIds":[ContractSectId],
            "LabIds":[LabId],
            "Pagination":{"rows":10,"page":1,"sidx":"","sord":""},
            "RegDateBegin":"",
            "RegDateEnd":""
        }

        # 发送请求
        resp = self.request(self.APIGetDaiRuKuPagingList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def sampleInStk(self, UserId, TestSampleMasterId):
        """样品确认入库"""
        # 请求参数
        data = {
             "OpUserId":UserId,
             "TestSampleMasterIds":[TestSampleMasterId],
             "InStkDate":time.strftime('%Y-%m-%d'),
             "InStkPlace": ranStr(5)
        }

        # 发送请求
        resp = self.request(self.APISampleInStk,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def sampleMasterRetention(self,UserId,UserFriendlyName,TestSampleMasterId,Type=1):
        """测前留样"""
        # 请求参数
        data = {
            "OpUserId": UserId,
            "TestSampleMasterIds":[TestSampleMasterId],
            "Type":Type, # 1 测前留样 2 测后留样
            "Date":time.strftime('%Y-%m-%d'),
            "UserID":None,
            "UserFriendlyName":UserFriendlyName,
            "Reason":None,
            "Period":"1",
            "Count":"1"
        }

        # 发送请求
        resp = self.request(self.APISampleMasterRetention,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getDaiJianPagingList(self,UserId,ContractSectId,LabId,Search,ProjectID=current_env['projectID']):
        """获取待检测样品列表"""
        # 请求参数
        data = {
            "ProjectID": ProjectID,
            "OpUserId":UserId,
            "MaterialTypeID":None,
            "Search":Search,
            "ContractSectIds":[ContractSectId],
            "LabIds":[LabId],
            "Pagination":{"rows":10,"page":1,"sidx":"","sord":""},
            "InStkDateBegin":"",
            "InStkDateEnd":""
        }

        # 发送请求
        resp = self.request(self.APIGetDaiJianPagingList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getRetentionPagingList(self, UserId, ContractSectId, LabId, Search,ProjectID=current_env['projectID']):
        """获取留样样品列表"""
        # 请求参数
        data = {
            "ProjectID": ProjectID,
            "OpUserId": UserId,
            "MaterialTypeID": None,
            "Search":Search,
            "ContractSectIds":[ContractSectId],
            "LabIds":[LabId],
            "Pagination":{"rows":10,"page":1,"sidx":"","sord":""},
            "RetentionDateBegin":"",
            "RetentionDateEnd":"",
            "Status":"0"
        }

        # 发送请求
        resp = self.request(self.APIGetRetentionPagingList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def sampleFlowCreateTask(self, UserId, ssoName, TestSampleMasterId, YangPinBH, ItemID):
        """在检样品创建试验"""
        # 请求参数
        data = {
            "OpUserId": UserId,
            "TestSampleMasterId": TestSampleMasterId,
            "YangPinBH": YangPinBH,
            "ItemID": ItemID,
            "ssoName":ssoName,
            "QualificationType":1
        }

        # 发送请求
        resp = self.request(self.APISampleFlowCreateTask,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp




sampleFlowManagementObj = SampleFlowManagement()
if __name__ == '__main__':
    resp = sampleFlowManagementObj.getDaiRuKuPagingList(759,269,'YP-[2023]333')
    print(resp)
    resp = sampleFlowManagementObj.sampleInStk('e5291001-2b37-41ea-bfda-d85167404874',6676)
    print(resp)


