# -*- coding: UTF-8  -*-
# @time     : 2023-09-13 15:57
# @Author   : Zhong Xue
# @File     : material_approach_management.py
import time

from api.baseApi import CommonAPI, commonAPIObj
from common.mySetting import current_env
from common.public import checkAPIResponse, ranInt, ranStr, getPassTime, executeJavaScript


class MaterialApproachManagement(CommonAPI):
    """材料进场登记"""

    def getMaterialType(self):
        """获取材料类型"""

        # 发送请求
        resp = self.request(self.APIGetMaterialType)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def createMaterialInput(self, contractId, materialTypeID,
                            certificateFileGuid=None,certificateNumber=None,
                            invoiceFileGuid=None,invoiceNumber=None,
                            otherFileGuid=None,otherNumber=None,
                            AttaKey=executeJavaScript('getGuid'),isUploadFile=True,IsAddPiHao=False):
        """材料进场登记"""
        # 请求参数
        data = {
            "ContractsectID": contractId,
            "MaterialTypeID": materialTypeID,
            "Price": ranInt(2),  # 结算单价
            "Name": ranStr(5),  # 材料名称
            "OutputCode": ranStr(5), # 生产批号
            "GuiGe": ranStr(5), # 规格型号
            "PlaceOrigin": ranStr(5), # 生产商
            "PrcdureCompany": ranStr(5),  # 供应商
            "InputDate": time.strftime('%Y-%m-%d'),  # 进场日期
            "PrcdureCompanyCode": ranStr(5), # 供应商编号
            "ProjectPart": ranStr(5), #拟用于部位
            "DateOfProduction": getPassTime(60).split(' ')[0],  # 生产日期
            "TaskQty":"1",
            "MaterialCount":"1",
            "Unit":"kg",
            "FacadeQuality": ranStr(5), # 外观质量
            "StorePlaceStr": ranStr(5), # 存放位置
            "Memo": ranStr(5), # 备注
            "AttaKey": AttaKey,
            "AttasInfo": []
        }
        if IsAddPiHao:
            data['PiHao'] = ranStr(5) # 牌号

        if isUploadFile:

            data["AttasInfo"] = [
                {
                    "FileGuid": certificateFileGuid,  # 合格证ID
                    "Number": certificateNumber
                },
                {
                    "FileGuid": invoiceFileGuid,  # 合格证ID
                    "Number": invoiceNumber
                },
                {
                    "FileGuid": otherFileGuid,  # 合格证ID
                    "Number": otherNumber
                }
            ]

        # 发送请求
        resp = self.request(self.APICreateMaterialInput,data)


        # 处理响应
        resp = checkAPIResponse(resp,data)

        # 返回结果
        return resp

    def getMaterialInput(self, contractId, materialTypeId, queryText='', projectID=current_env['projectID']):
        """查询材料进场登记信息"""
        # 请求参数
        data = {
            "ProjectID": projectID,
            "page": 1,
            "rows": 20,
            "queryText": queryText,
            "startInputDate": "",
            "endInputDate": "",
            "materialTypeId": materialTypeId,
            "contractId": contractId
        }

        # 发送请求
        resp = self.request(self.APIGetMaterialInput,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def updateMaterialInput(self,inData):
        """编辑材料进场信息"""
        # 请求参数
        # 发送请求
        resp = self.request(self.APIUpdateMaterialInput,inData)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def deleteMaterialInput(self,*args):
        """删除材料进场登记信息"""
        # 请求参数
        data = list(args)

        # 发送请求
        resp = self.request(self.APIDeleteMaterialInput,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def copyMaterialInput(self,inData):
        """复制材料进场登记信息"""
        # 请求参数
        # 发送请求
        resp = self.request(self.APICopyMaterialInput,inData)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getMaterialInputSignUserInfo(self,*taskID):
        """获取材料进场签字信息"""
        # 请求参数
        data = []
        for one in taskID:
            temp = {"taskID":one,"dataType":8,"foreignTable":"MaterialInput"}
            data.append(temp)

        # 发送请求
        resp = self.request(self.APIGetMaterialInputSignUserInfo,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def commitWorkFlow(self,inData):
        """提交材料进场登记"""
        # 请求参数
        # 发送请求
        resp = self.request(self.APICommitWorkFlow,inData)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

# -------------进一步封装以上方法------------------
    def commitMaterialInputInfo(self,*taskID):
        """材料进场登记信息提交"""
        resp = self.getMaterialInputSignUserInfo(*taskID)
        signUserInfo = resp['data']
        signUserInfo.pop('Name')
        for one in signUserInfo['SignItemList']:
            friendly_name_list = [o['FriendlyName'] for o in one['AllUserChildren']]
            if current_env['execUser'] in friendly_name_list:
                idx = friendly_name_list.index(current_env['execUser'])
                one['AllUserChildren'][idx]['IsChecked'] = 'true'
                one['UserChildren'] = one['AllUserChildren']
                one['selectUser'] = one['AllUserChildren'][idx]['UserId']
            else:
                one['AllUserChildren'][0]['IsChecked'] = 'true'
                one['UserChildren'] = one['AllUserChildren']
                one['selectUser'] = one['AllUserChildren'][0]['UserId']

        # 提交签字或审核
        self.commitWorkFlow(signUserInfo)


materialApproachManagementObj = MaterialApproachManagement()
if __name__ == '__main__':
    res = commonAPIObj.getLoginUserInfo()
    userId = res['userId']
    laboratoryID = res['laboratoryID']
    contractId = res['contractId']
    resp = materialApproachManagementObj.commitMaterialInputInfo(7031)
    print(resp)




