# -*- coding: UTF-8  -*-
# @time     : 2023-10-25 9:06
# @Author   : Zhong Xue
# @File     : experiment_sign.py
from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr


class ExperimentSign(CommonAPI):
    """试验签字"""

    def queryPdfTask(self, userId, contractSectID, labID, queryType, keyword, projectID=current_env['projectID']):
        """查询签字任务"""
        # 试验参数
        data = {
            "isTest": 1,
            "queryType": queryType, # 10 需要我签字 11 我已签过字 12 已签字完成
            "ProjectId": projectID,
            "userId": userId,
            "keyword": keyword,
            "SignStatus": "",
            "startDate": "",
            "endDate": "",
            "signCompletedDate": "",
            "qualificationType": 1,
            "contractSectIDs": contractSectID,
            "labIDs": labID,
            "PageSize": 30,
            "PageIndex": 1
        }

        # 发送请求
        resp = self.request(self.APIQueryPdfTask,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getAllSignLog(self,SignPdfTaskId):
        """获取签字任务日志信息"""
        # 请求参数
        data = {
            "SignPdfTaskIds": SignPdfTaskId,
            "PrevQueryTime": ""
        }

        # 发送请求
        resp = self.request(self.APIGetAllSignLog,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def pdfSignGen(self, foreignTable, foreignKey, pdfUserTaskId):
        """PDF转换"""
        # 请求参数
        data = [{
            "foreignTable":foreignTable,
            "foreignKey":foreignKey,
            "pdfUserTaskId": pdfUserTaskId
        }]

        # 发送请求
        resp = self.request(self.APIPdfSignGen,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getSignStatus(self,SignPdfUserTaskId):
        """获取签字状态"""
        # 请求参数
        data = {"SignPdfUserTaskId":[SignPdfUserTaskId]}

        # 发送请求
        resp = self.request(self.APIGetSignStatus,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def queryTaskProcess(self,taskID):
        """查询签字任务进程"""
        # 请求参数
        data = {"taskID":taskID,"dataType":"1"}

        # 发送请求
        resp = self.request(self.APIQueryTaskProcess,data)


        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def execPdfSign(self, foreignTable, foreignKey, pdfUserTaskId, projectID=current_env['projectID']):
        """执行签字"""
        # 请求参数
        payload1 = {
            "signAuthorizeCodeId": None,
            "projectId": projectID
        }

        payload2 = [{
            "foreignTable": foreignTable,
            "foreignKey": foreignKey,
            "pdfUserTaskId": pdfUserTaskId,
            "SignOpinionJsonList":[]
        }]

        # 发送请求
        resp = self.request(self.APIExecPdfSign, payload1, payload2)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def execPdfBack(self, foreignTable, foreignKey):
        """签字退回"""
        # 请求参数
        data = [{
            "foreignTable":foreignTable,
            "foreignKey": foreignKey,
            "taskID": foreignKey,
            "dataType":1,
            "RejectReason": ranStr(5)
        }]

        # 发送请求
        resp = self.request(self.APIExecPdfBack,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp




experimentSignObj = ExperimentSign()
if __name__ == '__main__':
    # resp = experimentSignObj.queryPdfTask("e5291001-2b37-41ea-bfda-d85167404874","759","269",10,"76204")
    # print(resp['data']['rows'][0])
    # resp = experimentSignObj.getAllSignLog("617b2cc3-3f08-4418-b344-cf648561c5af")
    # print(resp)
    # res = experimentSignObj.pdfSignGen("TestTask", "", "")
    # print(res)
    # experimentSignObj.execPdfSign("TestTask", "76210", "969412d3-4f49-4bef-b82a-ca152da80a69")
    resp = experimentSignObj.execPdfBack("TestTask", "76211")
    print(resp)