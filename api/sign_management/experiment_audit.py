# -*- coding: UTF-8  -*-
# @time     : 2023-10-25 9:06
# @Author   : Zhong Xue
# @File     : experiment_audit.py
import json
import time

from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr


class ExperimentAudit(CommonAPI):
    """试验审核"""

    def queryApproveTask(self, userId, contractSectID, labID, keyword, queryType=2, projectID=current_env['projectID']):
        """查询试验审核信息"""
        # 请求参数
        data = {
            "isTest": 1,
            "queryType": queryType,  # 2-需要我审核 3-我已审核过的 4-已审核完成的 5- 试验数据已处理完成的
            "ProjectId": projectID,
            "userId": userId,
            "keyword": keyword,
            "startDate": "",
            "endDate": "",
            "qualificationType": 1,
            "contractSectIDs": contractSectID,
            "labIDs": labID,
            "PageSize": 20,
            "PageIndex": 1
        }

        # 发送请求
        resp = self.request(self.APIQueryApproveTask,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def getSignTaskHistory(self,taskID):
        """查看试验任务签字或审核日志"""
        queryJson = {"taskID":taskID, "dataType":1}

        # 请求参数
        data = {
            "queryJson": json.dumps(queryJson)
        }

        # 发送请求
        resp = self.request(self.APIGetSignTaskHistory,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def execTaskAudit(self,foreignKey,foreignTable,isYes="true"):
        """试验审核"""
        # 请求参数
        payload1 = {"isYes": isYes}   # isYes true 审核通过 false 审核不通过
        payload2 = [{
            "foreignTable": foreignTable,
            "foreignKey": foreignKey,
            "signDate": time.strftime('%Y-%m-%d'),
            "signComment": ranStr(5)
        }]

        # 发送请求
        resp = self.request(self.APIExecTaskAudit,payload1,payload2)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp

    def execTaskAuditBack(self, foreignKey, foreignTable):
        """试验退回"""
        # 请求参数
        data = [{
            "foreignTable": foreignTable,
            "foreignKey": foreignKey,
            "taskID": foreignKey,
            "dataType": 1,
            "RejectReason": ranStr(5)
        }]
        # 发送请求
        resp = self.request(self.APIExecTaskAuditBack,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        # 返回结果
        return resp


experimentAuditObj = ExperimentAudit()
if __name__ == '__main__':
    # resp = experimentAuditObj.queryApproveTask("e5291001-2b37-41ea-bfda-d85167404874","759","269","76144",5)
    # print(resp["data"]["rows"][0]["UnSignCount"] == 0)
    # print(resp["data"]["rows"][0]["ForeignKey"] == "76144")
    resp = experimentAuditObj.getSignTaskHistory("76142")
    print(resp)
    # resp = experimentAuditObj.execTaskAudit("75948","TestTask")
    # print(resp)

