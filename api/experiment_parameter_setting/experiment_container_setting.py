# -*- coding: UTF-8  -*-
# @time     : 2023-10-18 10:18
# @Author   : Zhong Xue
# @File     : experiment_container_setting.py
import time

from api.baseApi import CommonAPI
from common.public import checkAPIResponse, ranStr


class ExperimentContainerSetting(CommonAPI):
    """试验容器设置"""

    def addFormTestParam(self, labid, MainType):
        """新增试验容器"""
        # 请求参数
        payload1 = {"labid": labid}
        payload2 = {
                "ChildType": ranStr(5),
                "ParamsID": 0,
                "MainType": MainType,
                "labid": labid,
                "LaboratoryID": labid
           }
        # 发送请求
        resp = self.request(self.APIFormTestParam, payload1, payload2)

        # 处理响应
        resp = checkAPIResponse(resp,payload2)

        return resp

    def getParamList(self,LabID):
        """获取参数"""
        # 请求参数
        data = {"LabID": LabID}

        # 发送请求
        resp = self.request(self.APIGetParamList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def addFormTestParamDetail(self,ParamsID):
        """新增校验信息"""
        # 请求参数
        data = {
            "TypeValueA": ranStr(5),
            "TypeValueB": ranStr(5),
            "VerifyDate":time.strftime('%Y-%m-%d'),
            "ParamsID": ParamsID,
            "ID":0
        }

        # 发送请求
        resp = self.request(self.APIFormTestParamDetail,data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        return resp

    def deleteTestDetailParam(self,cid):
        """删除试验校验信息"""
        # 请求参数
        data = {"cid": cid}

        # 发送请求
        resp = self.request(self.APIDeleteTestDetailParam,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def deleteTestParam(self,pid):
        """删除试验校验信息"""
        # 请求参数
        data = {"pid": pid}

        # 发送请求
        resp = self.request(self.APIDeleteTestParam,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp




experimentContainerSettingObj = ExperimentContainerSetting()
if __name__ == '__main__':
    resp = experimentContainerSettingObj.addFormTestParam(269,"盒质量")
    print(resp)

