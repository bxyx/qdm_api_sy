# -*- coding: UTF-8  -*-
# @time     : 2023-10-18 11:08
# @Author   : Zhong Xue
# @File     : custom_sieve_setting.py
from api.baseApi import CommonAPI
from common.mySetting import current_env
from common.public import checkAPIResponse, ranStr


class CustomSieveSetting(CommonAPI):
    """自定义筛孔设置"""

    def saveSieve(self, ContractSectID):
        """自定义筛孔设置"""
        # 请求参数
        data = {"ID":0, "Name": ranStr(5), "ContractSectID": ContractSectID}

        # 发送请求
        resp = self.request(self.APISaveSieve,data)

        # 处理响应
        resp = checkAPIResponse(resp, data)

        return resp

    def getSieveList(self, ContractId, projectID=current_env['projectID']):
        """获取自定义筛孔列表"""
        # 请求参数
        data = {
            "ProjectID": projectID,
            "ContractIds": ContractId
        }
        # 发送请求
        resp = self.request(self.APIGetSieveList,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def saveSieveDetail(self,sieveID):
        """新增筛孔明细"""
        # 请求参数
        data = {
            "Name": ranStr(5),
            "ID":0,
            "ShaiKongTypeID":sieveID,
            "MinNum": ranStr(5),
            "MaxNum": ranStr(5),
            "StandardNum": ranStr(5),
            "ControlAreaMax": ranStr(5),
            "ControlAreaMin": ranStr(5),
            "ControlPointMax": ranStr(5),
            "ControlPointMin": ranStr(5)
        }
        # 发送请求
        resp = self.request(self.APISaveSieveDetail, data)

        # 处理响应
        resp = checkAPIResponse(resp,data)

        return resp

    def getSieveDetailList(self, TypeId):
        """获取自定义筛孔明细"""
        # 请求参数
        data = {"TypeId": TypeId}

        # 发送请求
        resp = self.request(self.APIGetSieveDetailList, data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp

    def deleteSieve(self, typeId):
        """删除自定义筛孔"""
        # 请求参数
        data = {"typeId": typeId}

        # 处理响应
        resp = self.request(self.APIDeleteSieve,data)

        # 处理响应
        resp = checkAPIResponse(resp)

        return resp


customSieveSettingObj = CustomSieveSetting()
if __name__ == '__main__':
    resp = customSieveSettingObj.saveSieve(759)
    print(resp)

