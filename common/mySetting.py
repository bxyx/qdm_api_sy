# -*- coding: UTF-8  -*-
# @time     : 2023-03-07 10:21
# @Author   : Zhong Xue
# @File     : mySetting.py
import os

# 文件根路径
import time

from common.readYml import readYml

# 项目跟路径
rootPath = os.path.dirname(os.path.dirname(__file__))

# 测试环境yml文件路径
envYmlPath = os.path.join(rootPath,'conf','environment.yml')

# 指向当前环境
env = "test"
company = "test"
current_env = readYml(envYmlPath)[env][company]

# 接口yml文件路径
apiYmlPath = os.path.join(rootPath,'conf','apiConf.yml')

# 日志文件地址
logPath = os.path.join(rootPath, "logs", f'{time.strftime("%Y%m%d%H%M")}.log')

#js文件地址
jsFilePath = os.path.join(rootPath, "tools", "external.js")

# 测试结果，可读性报告输出路径
reportPath = os.path.join(rootPath,"report","%s" % company)
testResultPath = os.path.join(rootPath,"result","%s" % company)


# 外部文件地址
externalFilePath = os.path.join(rootPath, "externalFile")

if __name__ == '__main__':
    # print('%s\%s' % (externalFilePath,'试验数据.xls'))
    # print(os.path.join(externalFilePath,'standard.pdf'))
    print(current_env['execUser'])
