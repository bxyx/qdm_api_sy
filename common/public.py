# -*- coding: UTF-8  -*-
# @time     : 2023-03-07 13:49
# @Author   : Zhong Xue
# @File     : public.py
import datetime
import xlwings as xw
import random
import string
import time
import traceback

import execjs
import requests

from common.log import log
from common.mySetting import jsFilePath, current_env, externalFilePath


def executeJavaScript(fun_name,path=jsFilePath):
    """执行js"""
    # 打开并读取js文件(注意编码格式)
    js_file = open(path,'r',encoding='UTF-8')

    # 通过execjs库加载并编辑js文件
    res = execjs.compile(js_file.read()).call(fun_name)
    return res

def ranStr(num):
    """ 随机生成 num 个字符串 """
    str1 = 'Auto_'
    str2 = string.ascii_letters  # 返回26个英文大小写字母的字符串
    str3 = string.digits  # 返回阿拉伯数字的字符串
    # str4 = round(time.time() * 1000)  # 毫秒级时间戳
    salt = "".join(random.sample("%s%s" % (str2, str3), num))
    data = '%s%s' % (str1, salt)
    return data


def ranInt(num):
    """生成随机的num个数字的字符串"""
    digit_str = string.digits.replace('0','')  # 返回阿拉伯数字的字符串(去掉0)
    salt = "".join(random.sample(digit_str*2, num))
    return salt

def getDeltaTime(days,hour=23,minute=59,second=59):
    now_time = datetime.datetime.now()
    replace_time = now_time.replace(hour=hour, minute=minute, second=second)
    delta_time = datetime.timedelta(days=days)
    return (replace_time + delta_time).strftime('%Y-%m-%d %H:%M:%S')

def getPassTime(days,hour=23,minute=59,second=59):
    now_time = datetime.datetime.now()
    replace_time = now_time.replace(hour=hour, minute=minute, second=second)
    delta_time = datetime.timedelta(days=days)
    return (replace_time - delta_time).strftime('%Y-%m-%d %H:%M:%S')


def generate_id_number():
    """随机生成身份证号"""

    # 1、生成地址码
    # address_code = str(random.randint(110000,659004))
    address_code = "500112"

    # 2、出生年月日
    current_year = int(time.strftime('%Y')) - 1
    birth_date = str(random.randint(1950,current_year)) + str(random.randint(1,12)).zfill(2) + str(random.randint(1, 28)).zfill(2)  # zfill() 用于返回指定长度的字符串

    # 3、3位顺序码
    sequence_code = str(random.randint(1,999)).zfill(3)

    # 4、计算校验码
    id_number = address_code + birth_date + sequence_code

    # weights = [int(i) for i in range(10,1,-1)] + [int(i) for i in range(9,1,-1)]
    weights = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]

    check_sum = sum([int(id_number[i]) * weights[i] for i in range(17)]) % 11

    check_sum = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'][check_sum]

    id_number += check_sum

    return id_number


def checkAPIResponse(resp,returnData=None):
    """
    检查接口响应并返回相应数据
    :param resp: 接口响应
    :param returnData: 接口返回值
    :return:
    """
    url = resp.url.replace(current_env["host"],'').split('?')[0]
    status = resp.status_code
    log.info('接口 %s 响应状态码为 %s' % (url,status))

    try:
        resp.raise_for_status()
    except requests.exceptions.HTTPError as e:   # 捕获4xx或5xx异常并将结果输出到日志中
        log.error(traceback.format_exc())

    else:
        if resp.status_code == 200:  # 接口正确响应
            contentType = resp.headers.get('content-type')
            if not contentType:
                return None

            if 'application/json' in contentType:
                if isinstance(resp.json(),dict):
                    if resp.json().get('state') is not None and resp.json()['state'] != 1:
                        log.error('接口 %s 响应信息为: %s ' % (url, resp.json()['message']))  # 将错误信息输出到日志
                        return None

                    elif resp.json().get('success') is not None and not resp.json()['success']:
                        log.error('接口 %s 响应信息为: %s ' % (url, resp.json()['message']))  # 将错误信息输出到日志
                        return None

                    else:
                        # log.info('接口 %s 响应信息为: %s' % (url, resp.json()))
                        return  returnData if returnData is not None else resp.json()

                elif isinstance(resp.json(),list):

                    # log.info('接口 %s 响应信息为: %s' % (url, resp.json()))
                    return returnData if returnData is not None else resp.json()

                elif isinstance(resp.json(), str):
                    # log.info('接口 %s 响应信息为: %s' % (url, resp.json()))
                    return returnData if returnData is not None else resp.json()

                elif isinstance(resp.json(), bool):

                    # log.info('接口 %s 响应信息为: %s' % (url, resp.json()))
                    return returnData if returnData is not None else resp.json()

                elif isinstance(resp.json(), int):

                    # log.info('接口 %s 响应信息为: %s' % (url, resp.json()))
                    return returnData if returnData is not None else resp.json()

            elif 'text' in contentType:

                # log.info('接口 %s 响应信息为: %s' % (url, resp.text))   # inspect.stack()[1][3]
                return resp.text

            elif 'application/octet-stream' in contentType:

                return resp.content


def getExcelData(fileName,sheetName='sheet1'):
    """获取excel文件内容"""
    #with xw.App(visible=False, add_book=False) as app:
    app = xw.App(visible=False, add_book=False)
    wb = app.books.open(r'%s\%s' % (externalFilePath, fileName))
    ws = wb.sheets[sheetName]
    nrows = ws.used_range.last_cell.row
    ncols = ws.used_range.last_cell.column
    ncols = xw.utils.col_name(ncols)
    titleRow = ws.range('A1:M1').value
    resultData = []
    for i in range(2, nrows + 1):
        rowData = ws.range(f'A{i}:{ncols}{i}').value
        temp = dict()
        for k, v in zip(titleRow, rowData):
            temp[k] = v
            resultData.append(temp)
    wb.close()
    app.quit()
    return resultData


if __name__ == '__main__':
    res = executeJavaScript('getGuid')
    # print(res)
    # res = generate_id_number()
    print(res)



