# -*- coding: UTF-8  -*-
# @Time:2021-08-26 15:48
# @Author:zx

import logging

from common.mySetting import logPath


def logger(fileLog=True, name=__name__):

    # 1- 创建一个日志对象
    logger_obj = logging.getLogger(name)
    # 2- 设置日志的级别
    logger_obj.setLevel(logging.INFO)
    # 3- 日志内容格式对象
    formater = logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[%(lineno)d]:%(message)s")

    if fileLog:
        # 4- 创建日志输出对象 文件输出
        fh = logging.FileHandler(logPath, encoding='utf-8')  # 文件输出
        fh.setFormatter(formater)
        logger_obj.addHandler(fh)

    else:
        # 4- 创建日志输出对象 控制台输出
        sh = logging.StreamHandler()
        sh.setFormatter(formater)
        logger_obj.addHandler(sh)

    return logger_obj

log = logger()
if __name__ == '__main__':
    log.error('我是日志信息')
