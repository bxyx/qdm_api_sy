# -*- coding: UTF-8  -*-
# @time     : 2023-03-07 10:20
# @Author   : Zhong Xue
# @File     : readYaml.py
import yaml


def readYml(path):
    # 1.读取文件，2将文件内容转成python格式
    with open(path,encoding='utf-8') as f:
        return yaml.safe_load(f.read())


if __name__ == '__main__':
    res = readYml(r'E:\自动化测试代码\qdm\接口\AutoAPI_QDM_质检系统\conf\apiConf.yml')
    print(res['ProjectManagement']['APIAddProject']['data'])