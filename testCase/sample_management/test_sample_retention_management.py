# -*- coding: UTF-8  -*-
# @time     : 2023-09-19 10:38
# @Author   : Zhong Xue
# @File     : test_sample_retention_management.py
import time

import allure
import pytest
import xlwings as xw
from api.sample_management.sample_retention_management import sampleRetentionManagementObj
from common.mySetting import externalFilePath
from common.public import getExcelData



@allure.epic("QDM_API")
@allure.feature("样品管理")
@allure.story("样品留样登记")
@pytest.mark.pass_menu_address('/sampleManage/sampleRetention')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestSampleRetentionManagement:
    """测试样品留样登记"""

    @allure.title("测试样品留样登记")
    def test_sample_retention_001(self,fixtureCreateSampleMaster,fixtureGetLoginUserInfo):
        """测试样品留样登记"""
        userId = fixtureGetLoginUserInfo['userId']
        userName = fixtureGetLoginUserInfo['userName']
        contractId = fixtureGetLoginUserInfo['contractId']
        testSampleMasterId = fixtureCreateSampleMaster['ID']
        sampleCode = fixtureCreateSampleMaster['SampleCode']
        sampleName = fixtureCreateSampleMaster['SampleName']

        with allure.step("新增样品留样"):
            addedSampleRetentionInfo = sampleRetentionManagementObj.addSampleRetention(userId,contractId,testSampleMasterId,sampleCode,sampleName)

        with allure.step("查询样品留样信息"):
            resp = sampleRetentionManagementObj.getSampleRetentionPageList(userId,contractId,addedSampleRetentionInfo['SampleCode'])

            retentionId = resp['rows'][0]['Id']
            # 断言
            pytest.assume(resp['rows'][0]['TestSampleMasterId'] == addedSampleRetentionInfo['TestSampleMasterId'])
            pytest.assume(resp['rows'][0]['SampleCode'] == addedSampleRetentionInfo['SampleCode'])
            pytest.assume(resp['rows'][0]['SampleName'] == addedSampleRetentionInfo['SampleName'])
            pytest.assume(resp['rows'][0]['SampleName'] == addedSampleRetentionInfo['SampleName'])

        with allure.step("留样延期"):
            sampleRetentionInfo = sampleRetentionManagementObj.sampleRetentionDelay(userId, userName, retentionId)

        with allure.step("查询样品留样信息"):
            resp = sampleRetentionManagementObj.getSampleRetentionPageList(userId, contractId,
                                                                           addedSampleRetentionInfo['SampleCode'])
            # 断言
            pytest.assume(resp['rows'][0]['HandleType'] == "留样延期")
            pytest.assume(resp['rows'][0]['Period'] == sampleRetentionInfo['Period'])
            pytest.assume(resp['rows'][0]['Type'] == sampleRetentionInfo['Type'])

        with allure.step("留样废弃"):
            sampleRetentionManagementObj.sampleRetentionAbandonV2(userId, userName, retentionId)


        with allure.step("查询样品留样信息"):
            resp = sampleRetentionManagementObj.getSampleRetentionPageList(userId, contractId,
                                                                      addedSampleRetentionInfo['SampleCode'])

            # 断言
            pytest.assume(resp['rows'][0]['HandleType'] == "留样提前废弃")
            pytest.assume(resp['rows'][0]['StatusDesc1'] == "已处理")

        with allure.step("导出excel"):
            resp = sampleRetentionManagementObj.getSampleRetentionListToExcel()

            with open(r'%s\%s' % (externalFilePath,'样品留样.xls'), 'wb+') as f:
                f.write(resp)

        # with allure.step('读取excel文件内容'):
        #     print(1)
        #     resp = getExcelData(fileName='样品留样.xls')
        #     flag = 0
        #
        #     print(flag)
            # for one in resp:
            #     if one['样品编号'] == addedSampleRetentionInfo['SampleCode']:
            #         flag = 1
            #         break
            # pytest.assume(flag == 1)




if __name__ == '__main__':
    pytest.main(["test_sample_retention_management.py","-s"])