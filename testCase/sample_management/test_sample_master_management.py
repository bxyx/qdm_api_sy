# -*- coding: UTF-8  -*-
# @time     : 2023-09-18 9:45
# @Author   : Zhong Xue
# @File     : test_sample_master_management.py
import time

import allure
import pytest

from api.sample_management.sample_master_management import sampleMasterManagementObj
from common.mySetting import current_env
from common.public import ranStr, getPassTime


@allure.epic("QDM_API")
@allure.feature("样品管理")
@allure.story("材料取样登记")
@pytest.mark.pass_menu_address('/web/MaterialSampling.html')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestSampleMasterManagement:
    """测试材料取样登记"""

    @allure.title("测试材料进场取样")
    @pytest.mark.project_param(Iswitness='WitnessConfirmEnabled', IsSampling='IsSamplingFromMaterialEntry')
    def test_material_input_sampling_001(self, fixtureGetLoginUserInfo, fixtureCreateMaterialInput,
                                 fixtureGetMaterialTypeProjectTree, fixtureGetProjectParamValue):
        """测试材料进场取样"""
        userId = fixtureGetLoginUserInfo['userId']
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        contractId = fixtureGetLoginUserInfo['contractId']

        if fixtureGetProjectParamValue['IsSamplingFromMaterialEntry'] != '1':
            pytest.skip("系统控制参数【是否开启见证取样流程】设置为否")

        else:
            testItemId = fixtureGetMaterialTypeProjectTree[current_env['materialType']]
            sampleCode = sampleMasterManagementObj.getSampleMasterNumber(contractId, testItemId)['data']

            # 获取见证人
            if fixtureGetProjectParamValue['WitnessConfirmEnabled'] == '1':
                resp = sampleMasterManagementObj.getWitnessList()
                witnessCompany = resp['data'][0]['LaboratoryName']
                witnessPerson = resp['data'][0]['UserList'][0]['FriendlyName']
                witnessUserId = resp['data'][0]['UserList'][0]['UserId']
            else:
                resp = sampleMasterManagementObj.getDefaultWitnessUser(fixtureCreateMaterialInput['MaterialTypeID'])
                witnessCompany = resp['data']['WitnessOrgs']
                witnessPerson = resp['data']['witnessPerson']
                witnessUserId = resp['data']['WitnessUserId']


            with allure.step("直接取样"):
                sampleMasterManagementObj.createSampleMaster(userId, laboratoryID, contractId, testItemId,
                                                             fixtureCreateMaterialInput['MaterialTypeID'],
                                                             fixtureCreateMaterialInput['ID'],
                                                             fixtureCreateMaterialInput['Name'], sampleCode,
                                                             fixtureCreateMaterialInput['GuiGe'],
                                                             fixtureCreateMaterialInput['Unit'],
                                                             fixtureCreateMaterialInput['ProjectPart'],
                                                             witnessCompany, witnessPerson, witnessUserId,
                                                             fixtureCreateMaterialInput['StorePlaceStr'],
                                                             fixtureCreateMaterialInput['PrcdureCompany'],
                                                             fixtureCreateMaterialInput['DateOfProduction'],
                                                             fixtureCreateMaterialInput['OutputCode'],
                                                             fixtureCreateMaterialInput['InputDate'],
                                                             fixtureCreateMaterialInput['FacadeQuality'],
                                                             fixtureCreateMaterialInput['Memo'])

            with allure.step("获取材料取样登记信息"):
                resp = sampleMasterManagementObj.getSampleMasterList(laboratoryID, contractId,
                                                                     fixtureCreateMaterialInput['MaterialTypeID'],sampleCode)

                # 断言
                pytest.assume(resp['rows'][0]['SampleName'] == fixtureCreateMaterialInput['Name'])
                pytest.assume(resp['rows'][0]['SampleCode'] == sampleCode)

    @allure.title("测试直接取样")
    @pytest.mark.project_param(Iswitness='WitnessConfirmEnabled')
    def test_direct_sampling_001(self, fixtureGetLoginUserInfo, fixtureGetMaterialType,
                                 fixtureGetMaterialTypeProjectTree, fixtureGetProjectParamValue):
        """测试直接取样"""
        materialTypeId = fixtureGetMaterialType['ID']
        userId = fixtureGetLoginUserInfo['userId']
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        contractId = fixtureGetLoginUserInfo['contractId']
        testItemId = fixtureGetMaterialTypeProjectTree[current_env['materialType']]
        sampleCode = sampleMasterManagementObj.getSampleMasterNumber(contractId, testItemId)['data']

        # 获取见证人
        if fixtureGetProjectParamValue['WitnessConfirmEnabled'] == '1':
            resp = sampleMasterManagementObj.getWitnessList()
            witnessCompany = resp['data'][0]['LaboratoryName']
            witnessPerson = resp['data'][0]['UserList'][0]['FriendlyName']
            witnessUserId = resp['data'][0]['UserList'][0]['UserId']
        else:
            resp = sampleMasterManagementObj.getDefaultWitnessUser(materialTypeId)
            witnessCompany = resp['data']['WitnessOrgs']
            witnessPerson = resp['data']['witnessPerson']
            witnessUserId = resp['data']['WitnessUserId']


        with allure.step("直接取样"):
            sampleMasterInfo = sampleMasterManagementObj.createSampleMaster(userId, laboratoryID, contractId, testItemId,
                                                         materialTypeId,None,ranStr(5), sampleCode,ranStr(5),"kg",ranStr(5),
                                                         witnessCompany, witnessPerson, witnessUserId,
                                                         ranStr(5),ranStr(5),getPassTime(60).split(' ')[0],
                                                         ranStr(5),time.strftime('%Y-%m-%d'),ranStr(5),ranStr(5))

        with allure.step("获取材料取样登记信息"):
            resp = sampleMasterManagementObj.getSampleMasterList(laboratoryID, contractId,
                                                                 materialTypeId,
                                                                 sampleCode)
            ID = resp['rows'][0]['ID']
            # 断言
            pytest.assume(resp['rows'][0]['SampleName'] == sampleMasterInfo['SampleName'])
            pytest.assume(resp['rows'][0]['SampleCode'] == sampleCode)

        with allure.step("复制粘贴材料取样登记"):
            resp = sampleMasterManagementObj.copySampleMaster(userId, laboratoryID, ID)

            # 断言
            pytest.assume(resp['state'] == 1)

        with allure.step("获取材料取样登记信息"):
            resp = sampleMasterManagementObj.getSampleMasterList(laboratoryID, contractId,
                                                                 materialTypeId,
                                                                 sampleMasterInfo['SampleName'])

            # 断言
            pytest.assume(len(resp['rows']) == 2)
            sampleMasterIDlist = list()
            for one in resp['rows']:
                pytest.assume(one['Specification'] == sampleMasterInfo['Specification'])
                sampleMasterIDlist.append(one['ID'])


            with allure.step("删除材料进场登记"):
                resp = sampleMasterManagementObj.deleteSampleMaster(*sampleMasterIDlist)
                # 断言
                pytest.assume(resp['state'] == 1)



if __name__ == '__main__':
    pytest.main(['test_sample_master_management.py::TestSampleMasterManagement','-s'])
