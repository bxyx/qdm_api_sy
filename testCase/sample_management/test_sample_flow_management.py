# -*- coding: UTF-8  -*-
# @time     : 2023-10-07 10:01
# @Author   : Zhong Xue
# @File     : test_sample_flow_management.py
import allure
import pytest

from api.sample_management.sample_flow_management import sampleFlowManagementObj
from common.mySetting import current_env

@allure.epic("QDM_API")
@allure.feature("样品管理")
@allure.story("材料流转")
@pytest.mark.pass_menu_address('/web/SampleCirculation.html')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestSampleFlowManagement:
    """测试样品流转"""

    def test_sample_in_storage_001(self,fixtureCreateSampleMaster,
                                   fixtureGetMaterialTypeProjectTree,
                                   fixtureGetLoginUserInfo):
        """样品入库"""
        ItemID = fixtureGetMaterialTypeProjectTree[current_env['materialType']]
        ssoName = fixtureGetLoginUserInfo['ssoName']
        ContractSectID = fixtureCreateSampleMaster['ContractSectID']
        UserLabID = fixtureCreateSampleMaster['UserLabID']
        SampleCode = fixtureCreateSampleMaster['SampleCode']
        UserID = fixtureCreateSampleMaster['UserID']
        ID = fixtureCreateSampleMaster['ID']
        RetentionPerson = fixtureCreateSampleMaster['RetentionPerson']

        with allure.step("获取待入库样品列表"):
            resp = sampleFlowManagementObj.getDaiRuKuPagingList(UserID,ContractSectID,UserLabID,SampleCode)
            # 断言
            pytest.assume(ID == resp['rows'][0]['ID'])
            pytest.assume(SampleCode == resp['rows'][0]['SampleCode'])

        with allure.step("确认入库"):
            sampleFlowManagementObj.sampleInStk(UserID, ID)

        with allure.step("查询待检测样品"):
            resp = sampleFlowManagementObj.getDaiJianPagingList(UserID,ContractSectID,UserLabID,SampleCode)

            # 断言
            pytest.assume(ID == resp['rows'][0]['ID'])
            pytest.assume(SampleCode == resp['rows'][0]['SampleCode'])

        with allure.step("测前留样"):
            sampleFlowManagementObj.sampleMasterRetention(UserID, RetentionPerson, ID)


        with allure.step("留样样品信息"):
            resp = sampleFlowManagementObj.getRetentionPagingList(UserID,ContractSectID,UserLabID,SampleCode)
            # 断言
            pytest.assume(SampleCode == resp['rows'][0]['SampleCode'])

        with allure.step("待检测样品创建试验"):
            sampleFlowManagementObj.sampleFlowCreateTask(UserID, ssoName, ID, SampleCode, ItemID)


if __name__ == '__main__':
    pytest.main(["test_sample_flow_management.py::TestSampleFlowManagement","-s"])

