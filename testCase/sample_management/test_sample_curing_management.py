# -*- coding: UTF-8  -*-
# @time     : 2023-10-07 10:59
# @Author   : Zhong Xue
# @File     : test_sample_curing_management.py
import allure
import pytest

from api.sample_management.sample_curing_management import sampleCuringManagementObj


@allure.epic("QDM_API")
@allure.feature("样品管理")
@allure.story("材料养护登记")
@pytest.mark.pass_menu_address('/sampleManage/maintenanceSample')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestSampleCuringManagement:
    """"测试样品养护登记"""

    def test_create_curing_001(self,fixtureCreateSampleMaster):
        """新增样品养护信息"""
        ContractSectID = fixtureCreateSampleMaster['ContractSectID']
        SampleCode = fixtureCreateSampleMaster['SampleCode']
        SampleId = fixtureCreateSampleMaster['ID']
        UserID = fixtureCreateSampleMaster['UserID']
        SampleName = fixtureCreateSampleMaster['SampleName']

        with allure.step("查询样品信息"):
            resp = sampleCuringManagementObj.getTestSamplesForMaintenance(ContractSectID, SampleCode)

            # 断言
            pytest.assume(resp['data']['rows'][0]['SampleId'] == SampleId)
            pytest.assume(resp['data']['rows'][0]['SampleCode'] == SampleCode)

        with allure.step("创建样品养护信息"):
            sampleCuringManagementObj.sampleCreateMaintenance(UserID, ContractSectID, SampleCode, SampleId, SampleName)


        with allure.step("获取样品养护列表"):
            resp = sampleCuringManagementObj.getSampleMaintenanceList(ContractSectID, SampleCode)

            # 断言
            pytest.assume(resp['data']['rows'][0]['SampleCode'] == SampleCode)
            pytest.assume(resp['data']['rows'][0]['SampleId'] == SampleId)
            pytest.assume(resp['data']['rows'][0]['Status'] == '养护中')
            Id = resp['data']['rows'][0]['Id']

        with allure.step("出库"):
            sampleCuringManagementObj.sampleMaintenanceDelivery(Id)

        with allure.step("获取样品养护列表"):
            resp = sampleCuringManagementObj.getSampleMaintenanceList(ContractSectID, SampleCode)

            # 断言
            pytest.assume(resp['data']['rows'][0]['Status'] == '已出库')


        with allure.step("删除样品养护登记"):
            sampleCuringManagementObj.sampleMaintenanceDelete(Id)


        with allure.step("获取样品养护列表"):
            resp = sampleCuringManagementObj.getSampleMaintenanceList(ContractSectID, SampleCode)

            # 断言
            pytest.assume(len(resp['data']['rows']) == 0)



if __name__ == '__main__':
    pytest.main(['test_sample_curing_management.py::TestSampleCuringManagement','-s'])