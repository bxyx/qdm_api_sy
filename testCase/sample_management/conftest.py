# -*- coding: UTF-8  -*-
# @time     : 2023-09-14 13:45
# @Author   : Zhong Xue
# @File     : conftest.py
import time

import pytest

from api.sample_management.material_approach_management import materialApproachManagementObj
from api.sample_management.sample_master_management import sampleMasterManagementObj
from common.mySetting import current_env
from common.public import ranStr, getPassTime


@pytest.fixture(scope='module')
def fixtureCreateMaterialInput(fixtureGetMaterialType,fixtureGetLoginUserInfo):
    """添加材料进场登记"""
    IsAddPiHao = fixtureGetMaterialType['IsAddPiHao']
    materialTypeId = fixtureGetMaterialType['ID']
    contractId = fixtureGetLoginUserInfo['contractId']

    addedMaterialInput = materialApproachManagementObj.createMaterialInput(contractId,materialTypeId,isUploadFile=False,IsAddPiHao=IsAddPiHao)

    # 查询材料进场信息
    resp = materialApproachManagementObj.getMaterialInput(contractId, materialTypeId, addedMaterialInput['GuiGe'])
    addedMaterialInput['ID'] = resp['data']['rows'][0]['ID']
    yield addedMaterialInput
    contractId = fixtureGetLoginUserInfo['contractId']
    # 删除材料进场登记信息
    resp = materialApproachManagementObj.getMaterialInput(contractId, '1')
    deleteIDList = []
    for one in resp['data']['rows']:
        deleteIDList.append(one['ID'])
    materialApproachManagementObj.deleteMaterialInput(*deleteIDList)







