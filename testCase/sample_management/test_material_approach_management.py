# -*- coding: UTF-8  -*-
# @time     : 2023-09-14 13:41
# @Author   : Zhong Xue
# @File     : test_material_approach_management.py
import json
import time

import allure
import pytest

from api.baseApi import commonAPIObj
from api.sample_management.material_approach_management import materialApproachManagementObj
from common.public import executeJavaScript, ranStr


@allure.epic("QDM_API")
@allure.feature("样品管理")
@allure.story("材料进场登记")
@pytest.mark.pass_menu_address('/web/MaterialEntry.html')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestMaterialApproachManagement:
    """测试材料进场登记"""

    @allure.title("测试新增材料进场登记")
    def test_create_material_input_001(self, fixtureGetMaterialType,fixtureGetLoginUserInfo):
        """测试新增材料进场登记"""
        IsAddPiHao = fixtureGetMaterialType['IsAddPiHao']
        materialTypeId = fixtureGetMaterialType['ID']
        contractId = fixtureGetLoginUserInfo['contractId']

        foreignKey = executeJavaScript('getGuid')
        certificateNumber = ranStr(5)
        invoiceNumber = ranStr(5)
        otherNumber  = ranStr(5)
        with allure.step("上传材料进场登记附件-合格证,发票,其他附件"):
            # # 上传发票附件
            uploadedCertificateFile = commonAPIObj.uploadAccessory(foreignKey, 'MaterialInput', '合格证', foreignKey2=certificateNumber)
            uploadedInvoiceFile = commonAPIObj.uploadAccessory(foreignKey, 'MaterialInput', '发票', foreignKey2=invoiceNumber)
            uploadedOtherFile = commonAPIObj.uploadAccessory(foreignKey, 'MaterialInput', "null", foreignKey2=otherNumber)

        with allure.step("新增材料进场登记"):
            addedMaterialInput = materialApproachManagementObj.createMaterialInput(contractId, materialTypeId,
                                                              uploadedCertificateFile['Id'],certificateNumber,
                                                              uploadedInvoiceFile['Id'],invoiceNumber,
                                                              uploadedOtherFile['Id'],otherNumber,foreignKey,
                                                              IsAddPiHao=IsAddPiHao)


        with allure.step("查询材料进场登记信息"):
            resp = materialApproachManagementObj.getMaterialInput(contractId, materialTypeId, addedMaterialInput['GuiGe'])

            # 断言
            pytest.assume(resp['data']['rows'][0]['Name'] == addedMaterialInput['Name'])
            pytest.assume(resp['data']['rows'][0]['OutputCode'] == addedMaterialInput['OutputCode'])
            pytest.assume(resp['data']['rows'][0]['GuiGe'] == addedMaterialInput['GuiGe'])
            pytest.assume(resp['data']['rows'][0]['PlaceOrigin'] == addedMaterialInput['PlaceOrigin'])
            pytest.assume(resp['data']['rows'][0]['PrcdureCompany'] == addedMaterialInput['PrcdureCompany'])
            pytest.assume(resp['data']['rows'][0]['PrcdureCompanyCode'] == addedMaterialInput['PrcdureCompanyCode'])
            pytest.assume(resp['data']['rows'][0]['ProjectPart'] == addedMaterialInput['ProjectPart'])
            pytest.assume(resp['data']['rows'][0]['FacadeQuality'] == addedMaterialInput['FacadeQuality'])
            pytest.assume(resp['data']['rows'][0]['StorePlaceStr'] == addedMaterialInput['StorePlaceStr'])
            pytest.assume(resp['data']['rows'][0]['Memo'] == addedMaterialInput['Memo'])

        with allure.step("查看材料进场登记附件信息"):
            # 获取合格证附件
            resp = commonAPIObj.getAccessory(foreignKey, "MaterialInput", "合格证")
            # 断言
            pytest.assume(uploadedCertificateFile['Url'] == resp[0]['Url'])
            pytest.assume(uploadedCertificateFile['Name'] == resp[0]['Name'])

            # 获取发票附件
            resp = commonAPIObj.getAccessory(foreignKey, "MaterialInput", "发票")
            # 断言
            pytest.assume(uploadedInvoiceFile['Url'] == resp[0]['Url'])
            pytest.assume(uploadedInvoiceFile['Name'] == resp[0]['Name'])

            # 获取其他附件
            resp = commonAPIObj.getAccessory(foreignKey, "MaterialInput", "null")
            # 断言
            pytest.assume(uploadedOtherFile['Url'] == resp[0]['Url'])
            pytest.assume(uploadedOtherFile['Name'] == resp[0]['Name'])


    @allure.title('测试编辑、复制、删除材料进程信息')
    def test_update_copy_material_input_001(self,fixtureGetMaterialType,fixtureGetLoginUserInfo,fixtureCreateMaterialInput):
        """测试编辑、复制、删除材料进程信息"""
        materialTypeId = fixtureGetMaterialType['ID']
        contractId = fixtureGetLoginUserInfo['contractId']
        with allure.step("编辑材料进场登记"):
            resp = materialApproachManagementObj.updateMaterialInput(fixtureCreateMaterialInput)

            # 断言
            pytest.assume(resp['state'] == 1)

        with allure.step("复制材料进场登记信息"):
            resp = materialApproachManagementObj.getMaterialInput(contractId, materialTypeId,fixtureCreateMaterialInput['GuiGe'])
            inData = resp['data']['rows'][0]

            resp = materialApproachManagementObj.copyMaterialInput([inData])
            # 断言
            pytest.assume(resp['state'] == 1)

        with allure.step('查询复制的材料登记信息'):
            resp = materialApproachManagementObj.getMaterialInput(contractId, materialTypeId,fixtureCreateMaterialInput['GuiGe'])
            pytest.assume(len(resp['data']['rows']) == 2)

    # @pytest.mark.project_param(Param1="IsOpenVerify",Params2="IsOpenSign",Params3="EnableMaterialInputCheck")
    # def test_submit_material_input_001(self,fixtureGetProjectParamValue,fixtureCreateMaterialInput):
    #     """提交材料进场登记信息"""
    #     # 判断是否开始审核或签字
    #
    #     if fixtureGetProjectParamValue['EnableMaterialInputCheck'] == '0' or fixtureGetProjectParamValue['IsOpenVerify'] == '0' and fixtureGetProjectParamValue['IsOpenSign'] == '0':
    #         pytest.skip(reason="未开始审核或签字系统控制参数")
    #     else:
    #         with allure.step("提交材料进场登记信息"):
    #             materialApproachManagementObj.commitMaterialInputInfo(fixtureCreateMaterialInput['ID'])


if __name__ == '__main__':
    pytest.main(["test_material_approach_management.py::TestMaterialApproachManagement","-s"])