# -*- coding: UTF-8  -*-
# @time     : 2023-10-25 9:38
# @Author   : Zhong Xue
# @File     : test_experiment_audit.py
import allure
import pytest

from api.baseApi import APILogIn, CommonAPI
from api.experiment_detection_management.experiment_detection import experimentDetectionObj
from api.sign_management.experiment_audit import experimentAuditObj, ExperimentAudit
from common.mySetting import company, current_env

@allure.epic("QDM_API")
@allure.feature("签字管理")
@allure.story("试验审核")
@pytest.mark.pass_menu_address('/web/SignTask-new.html')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestExperimentAudit():
    """测试试验审核"""

    @pytest.mark.project_param(IsOpenVerify="IsOpenVerify")
    @allure.title("测试试验审核通过")
    def test_experiment_audit_pass_001(self, fixtureGetLoginUserInfo, fixtureGetProjectParamValue, fixtureCompleteExperiment):
        """测试试验审核通过"""
        userID = fixtureGetLoginUserInfo['userId']
        contractID = fixtureGetLoginUserInfo['contractId']
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        taskID = fixtureCompleteExperiment['task']['TaskID']

        if fixtureGetProjectParamValue["IsOpenVerify"] != "1":
            pytest.skip(f"【{company}】未开启试验审核系统控制参数")

        else:
            with allure.step("试验审核"):
                for one in fixtureCompleteExperiment['taskProcess']:
                    if one['FriendlyName'] == current_env['execUser']:
                        # 查询待审核试验
                        resp = experimentAuditObj.queryApproveTask(userID, contractID, laboratoryID, taskID)
                        pytest.assume(resp['data']['total'] == 1)
                        ForeignKey = resp['data']['rows'][0]['TaskSignEntity']['ForeignTable']
                        # 试验审核
                        experimentAuditObj.execTaskAudit(taskID,ForeignKey)

                        # 查看试验日志信息
                        resp = experimentAuditObj.getSignTaskHistory(taskID)

                        pytest.assume(len(resp) > 0)
                    else:
                        # 待审核人员登录系统
                        token = APILogIn(one['FriendlyName'])
                        # 获取当前登录用户信息
                        userInfoDict = CommonAPI(token).getLoginUserInfo()
                        # 查询待审核试验
                        resp = ExperimentAudit(token).queryApproveTask(userInfoDict['userId'], contractID, laboratoryID, taskID)
                        pytest.assume(resp['data']['total'] == 1)
                        ForeignKey = resp['data']['rows'][0]['TaskSignEntity']['ForeignTable']
                        # 试验审核
                        ExperimentAudit(token).execTaskAudit(taskID,ForeignKey)

                        # 查看试验日志信息
                        resp = experimentAuditObj.getSignTaskHistory(taskID)

                        pytest.assume(len(resp) > 0)

            with allure.step("查询已审核完成的试验"):
                resp = experimentAuditObj.queryApproveTask(userID, contractID, laboratoryID, taskID, queryType=5)
                # 断言
                pytest.assume(resp["data"]["rows"][0]["UnSignCount"] == 0)
                pytest.assume(resp["data"]["rows"][0]["ForeignKey"] == str(taskID))

    @pytest.mark.project_param(IsOpenVerify="IsOpenVerify")
    @allure.title("测试试验审核不通过")
    def test_experiment_audit_no_pass_001(self, fixtureGetLoginUserInfo, fixtureGetProjectParamValue,
                                       fixtureCompleteExperiment):
        """测试试验审核不通过"""
        userID = fixtureGetLoginUserInfo['userId']
        ssoName = fixtureGetLoginUserInfo['ssoName']
        contractID = fixtureGetLoginUserInfo['contractId']
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        taskID = fixtureCompleteExperiment['task']['TaskID']

        if fixtureGetProjectParamValue["IsOpenVerify"] != "1":
            pytest.skip(f"【{company}】未开启试验审核系统控制参数")

        else:
            with allure.step("试验审核不通过"):
                unSignUser = fixtureCompleteExperiment['taskProcess'][0]

                if unSignUser['FriendlyName'] == current_env['execUser']:
                    # 查看试验日志信息
                    resp = experimentAuditObj.queryApproveTask(userID, contractID, laboratoryID, taskID)
                    pytest.assume(resp['data']['total'] == 1)
                    ForeignKey = resp['data']['rows'][0]['TaskSignEntity']['ForeignTable']

                    # 试验审核
                    experimentAuditObj.execTaskAudit(taskID, ForeignKey,"false")
                else:
                    # 待审核人员登录系统
                    token = APILogIn(unSignUser['FriendlyName'])
                    # 查询待审核试验
                    resp = ExperimentAudit(token).queryApproveTask(userID, contractID, laboratoryID, taskID)
                    pytest.assume(resp['data']['total'] == 1)
                    ForeignKey = resp['data']['rows'][0]['TaskSignEntity']['ForeignTable']
                    # 试验审核
                    ExperimentAudit(token).execTaskAudit(taskID, ForeignKey,"false")

            with allure.step("查询审核不通过的试验信息"):
                resp = experimentDetectionObj.getTestTaskListV2(ssoName, contractID, "", taskID)

                # 断言
                pytest.assume(resp["rows"][0]["TaskStateDesc"] == "审核退回")
                pytest.assume(resp["rows"][0]["TaskID"] == taskID)

    @pytest.mark.project_param(IsOpenVerify="IsOpenVerify")
    @allure.title("测试试验审核退回")
    def test_experiment_audit_send_back_001(self, fixtureGetLoginUserInfo, fixtureGetProjectParamValue,
                                          fixtureCompleteExperiment):
        """测试试验审核退回"""
        userID = fixtureGetLoginUserInfo['userId']
        ssoName = fixtureGetLoginUserInfo['ssoName']
        contractID = fixtureGetLoginUserInfo['contractId']
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        taskID = fixtureCompleteExperiment['task']['TaskID']

        if fixtureGetProjectParamValue["IsOpenVerify"] != "1":
            pytest.skip(f"【{company}】未开启试验审核系统控制参数")

        else:
            with allure.step("测试试验审核退回"):
                unSignUser = fixtureCompleteExperiment['taskProcess'][0]

                if unSignUser['FriendlyName'] == current_env['execUser']:
                    # 查看试验日志信息
                    resp = experimentAuditObj.queryApproveTask(userID, contractID, laboratoryID, taskID)
                    pytest.assume(resp['data']['total'] == 1)
                    ForeignKey = resp['data']['rows'][0]['TaskSignEntity']['ForeignTable']

                    # 审核退回
                    experimentAuditObj.execTaskAuditBack(taskID, ForeignKey)

                else:
                    # 待审核人员登录系统
                    token = APILogIn(unSignUser['FriendlyName'])
                    # 查询待审核试验
                    resp = ExperimentAudit(token).queryApproveTask(userID, contractID, laboratoryID, taskID)
                    pytest.assume(resp['data']['total'] == 1)
                    ForeignKey = resp['data']['rows'][0]['TaskSignEntity']['ForeignTable']

                    # 审核退回
                    ExperimentAudit(token).execTaskAuditBack(taskID, ForeignKey)

            with allure.step("查询审核不通过的试验信息"):
                resp = experimentDetectionObj.getTestTaskListV2(ssoName, contractID, "", taskID)

                # 断言
                pytest.assume(resp["rows"][0]["TaskStateDesc"] == "审核退回")
                pytest.assume(resp["rows"][0]["TaskID"] == taskID)

if __name__ == '__main__':
    pytest.main(["test_experiment_audit.py::TestExperimentAudit","-s"])





