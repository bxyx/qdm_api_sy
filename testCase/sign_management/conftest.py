# -*- coding: UTF-8  -*-
# @time     : 2023-10-25 9:46
# @Author   : Zhong Xue
# @File     : conftest.py
import time

import pytest

from api.baseApi import APILogIn, CommonAPI
from api.experiment_detection_management.experiment_detection import experimentDetectionObj
from api.sign_management.experiment_sign import experimentSignObj, ExperimentSign
from common.mySetting import current_env


@pytest.fixture(scope='function')
def fixtureCompleteExperiment(fixtureGetProjectParamValue, fixtureCreateExperiment,fixtureGetLoginUserInfo):
    """完成试验"""
    taskID = fixtureCreateExperiment['TaskID']
    ssoName = fixtureGetLoginUserInfo['ssoName']
    contractID = fixtureGetLoginUserInfo['contractId']

    isAuditOrSign = True if (fixtureGetProjectParamValue.get('IsOpenVerify') == '1' or fixtureGetProjectParamValue.get('IsOpenSign') == '1') else False
    # 完成试验
    experimentDetectionObj.commitMaterialInputInfo(taskID, isAuditOrSign)

    # 查询试验进度
    resp = experimentDetectionObj.queryTaskProcess(taskID)
    yield {"task": fixtureCreateExperiment,"taskProcess": resp['data']['ProcessItems']}
    # 查询处理完毕的试验
    resp = experimentDetectionObj.getTestTaskListV2(ssoName, contractID, "", taskID, 1, '2,4,5,7,9')
    if resp['rows'] != []:
        ForeignTable = resp['rows'][0]['ForeignTable']
        # 测回试验
        resp = experimentDetectionObj.execCancelWorkFlow(taskID,ForeignTable)
        pytest.assume(resp['details'][0]['TaskID'] == taskID)

    # 删除
    experimentDetectionObj.deleteTestTask(ssoName,taskID)

@pytest.fixture(scope='function')
def fixturePdfConversion(fixtureCompleteExperiment,fixtureGetLoginUserInfo,fixtureGetProjectParamValue):
    """PDF转换"""
    if fixtureGetProjectParamValue["IsOpenSign"] == "1":
        unSignUser = fixtureCompleteExperiment['taskProcess'][0]
        userID = fixtureGetLoginUserInfo['userId']
        contractID = fixtureGetLoginUserInfo['contractId']
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        taskID = fixtureCompleteExperiment['task']['TaskID']
        if unSignUser['FriendlyName'] == current_env['execUser']:
            # 查询待签字的试验任务
            resp = experimentSignObj.queryPdfTask(userID, contractID, laboratoryID, 10, taskID)
            pdfUserTaskId = resp['data']['rows'][0]['userTask']['ID']
        else:
            # 待审核人员登录系统
            token = APILogIn(unSignUser['FriendlyName'])
            userID = CommonAPI(token).getLoginUserInfo()['userId']
            resp = ExperimentSign(token).queryPdfTask(userID, contractID, laboratoryID, 10, taskID)
            pdfUserTaskId = resp['data']['rows'][0]['userTask']['ID']

        # PDF转换
        while 1:
            res = experimentSignObj.getSignStatus(pdfUserTaskId)
            time.sleep(2)
            print(res['data'][0]['TaskStatusDesc'])
            if res['data'][0]['TaskStatusDesc'] in ['PDF生成失败', '已生成','PDF已生成','签字中']:
                break
    yield fixtureCompleteExperiment


