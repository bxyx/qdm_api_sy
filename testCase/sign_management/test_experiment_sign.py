# -*- coding: UTF-8  -*-
# @time     : 2023-10-25 13:55
# @Author   : Zhong Xue
# @File     : test_experiment_sign.py
import allure
import pytest

from api.baseApi import APILogIn, CommonAPI
from api.experiment_detection_management.experiment_detection import experimentDetectionObj
from api.sign_management.experiment_sign import experimentSignObj, ExperimentSign
from common.mySetting import company, current_env

@allure.epic("QDM_API")
@allure.feature("签字管理")
@allure.story("试验签字")
@pytest.mark.pass_menu_address('/web/TestUkeyPDFSign-new.html')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestExperimentSign():
    """测试试验签字"""

    @pytest.mark.project_param(IsOpenSign="IsOpenSign")
    def test_experiment_sign_pass_001(self, fixtureGetProjectParamValue, fixtureGetLoginUserInfo, fixturePdfConversion):

        userID = fixtureGetLoginUserInfo['userId']
        contractID = fixtureGetLoginUserInfo['contractId']
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        taskID = fixturePdfConversion['task']['TaskID']

        if fixtureGetProjectParamValue["IsOpenSign"] != "1":
            pytest.skip(f"【{company}】未开启试验签字系统控制参数")

        else:
            with allure.step("试验签字"):
                for one in fixturePdfConversion['taskProcess']:
                    if one['FriendlyName'] == current_env['execUser']:
                        # 查询待签字试验
                        resp = experimentSignObj.queryPdfTask(userID, contractID,laboratoryID,10,taskID)
                        foreignTable = resp['data']['rows'][0]['ForeignTable']
                        foreignKey = resp['data']['rows'][0]['ForeignKey']
                        pdfUserTaskId = resp['data']['rows'][0]['userTask']['ID']
                        SignPdfTaskID = resp['data']['rows'][0]['userTask']['SignPdfTaskID']
                        # 试验签字
                        experimentSignObj.execPdfSign(foreignTable, foreignKey, pdfUserTaskId)
                        # 查看签字任务日志
                        resp = experimentSignObj.getAllSignLog(SignPdfTaskID)
                        # 断言
                        pytest.assume(resp['data'][0]['FriendlyName'] == one['FriendlyName'])
                    else:
                        # 待审核人员登录系统
                        token = APILogIn(one['FriendlyName'])
                        # 获取当前登录用户信息
                        userInfoDict = CommonAPI(token).getLoginUserInfo()
                        # 查询待签字试验
                        resp = ExperimentSign(token).queryPdfTask(userInfoDict['userId'], contractID, laboratoryID, 10, taskID)
                        foreignTable = resp['data']['rows'][0]['ForeignTable']
                        foreignKey = resp['data']['rows'][0]['ForeignKey']
                        pdfUserTaskId = resp['data']['rows'][0]['userTask']['ID']
                        SignPdfTaskID = resp['data']['rows'][0]['userTask']['SignPdfTaskID']
                        # 试验签字
                        ExperimentSign(token).execPdfSign(foreignTable, foreignKey, pdfUserTaskId)
                        # 查看签字任务日志
                        resp = ExperimentSign(token).getAllSignLog(SignPdfTaskID)
                        # 断言
                        pytest.assume(resp['data'][0]['FriendlyName'] == one['FriendlyName'])

    @pytest.mark.project_param(IsOpenSign="IsOpenSign")
    def test_experiment_sign_send_back_001(self, fixtureGetProjectParamValue, fixtureGetLoginUserInfo, fixtureCompleteExperiment):
        userID = fixtureGetLoginUserInfo['userId']
        ssoName = fixtureGetLoginUserInfo['ssoName']
        contractID = fixtureGetLoginUserInfo['contractId']
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        taskID = fixtureCompleteExperiment['task']['TaskID']

        if fixtureGetProjectParamValue["IsOpenSign"] != "1":
            pytest.skip(f"【{company}】未开启试验签字系统控制参数")

        else:
            with allure.step("试验签字退回"):
                unSignUser = fixtureCompleteExperiment['taskProcess'][0]
                if unSignUser['FriendlyName'] == current_env['execUser']:
                    # 查询待签字试验
                    resp = experimentSignObj.queryPdfTask(userID, contractID, laboratoryID, 10, taskID)
                    foreignTable = resp['data']['rows'][0]['ForeignTable']
                    foreignKey = resp['data']['rows'][0]['ForeignKey']

                    # 签字退回
                    experimentSignObj.execPdfBack(foreignTable, foreignKey)
                else:
                    # 待审核人员登录系统
                    token = APILogIn(unSignUser['FriendlyName'])
                    # 获取当前登录用户信息
                    userInfoDict = CommonAPI(token).getLoginUserInfo()
                    # 查询待签字试验
                    resp = ExperimentSign(token).queryPdfTask(userInfoDict['userId'], contractID, laboratoryID, 10, taskID)
                    foreignTable = resp['data']['rows'][0]['ForeignTable']
                    foreignKey = resp['data']['rows'][0]['ForeignKey']

                    # 签字退回
                    ExperimentSign(token).execPdfBack(foreignTable, foreignKey)

            with allure.step("查询审核不通过的试验信息"):
                resp = experimentDetectionObj.getTestTaskListV2(ssoName, contractID, "", taskID)

                # 断言
                pytest.assume(resp["rows"][0]["TaskStateDesc"] == "签字退回")
                pytest.assume(resp["rows"][0]["TaskID"] == taskID)


if __name__ == '__main__':
    pytest.main(['test_experiment_sign.py::TestExperimentSign','-s'])