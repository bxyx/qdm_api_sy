# -*- coding: UTF-8  -*-
# @time     : 2023-10-17 9:58
# @Author   : Zhong Xue
# @File     : test_experiment_detection_scan.py
import time

import allure
import pytest

from api.experiment_detection_management.consign_experiment_detection_management import \
    consignExperimentDetectionManagementObj


@allure.epic("QDM_API")
@allure.feature("试验检测管理")
@allure.story("试验检测扫描")
@pytest.mark.pass_data(isThirdTest='false',DataType='1')  #
@pytest.mark.pass_menu_address('/web/OutsourcingTestManage.html?dataform=1')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestExperimentDetectionScan:
    """测试试验检测扫描"""

    @allure.title("测试试验检测扫描")
    def test_create_consign_experiment_001(self,fixtureCreateConsignTest,fixtureCreateSampleMaster):
        UserUnitID = fixtureCreateSampleMaster['UserLabID']
        ContractSectID =  fixtureCreateSampleMaster['ContractSectID']
        ConsignTestClsID = fixtureCreateConsignTest['consignTestTreeId']
        TestSampleMasterID = fixtureCreateSampleMaster['ID']
        SampleName = fixtureCreateSampleMaster['SampleName']
        SampleCode = fixtureCreateSampleMaster['SampleCode']

        with allure.step("新增试验检测扫描"):
            addedConsignTest = consignExperimentDetectionManagementObj.createConsignTest(UserUnitID, ContractSectID,
                                                                            ConsignTestClsID,TestSampleMasterID,
                                                                            SampleName, SampleCode)

        with allure.step("查询试验检测扫描试验"):
            res = consignExperimentDetectionManagementObj.getConsignTestPageList(UserUnitID, ContractSectID,
                                                                                 addedConsignTest['ConsignNo'],ConsignTestClsID,DataType=1)
            # 断言
            pytest.assume(res['rows'][0]['ConsignNo'] == addedConsignTest['ConsignNo'])
            pytest.assume(res['rows'][0]['ReportNo'] == addedConsignTest['ReportNo'])
            pytest.assume(res['rows'][0]['SampleName'] == addedConsignTest['SampleName'])
            pytest.assume(res['rows'][0]['ProjectPart'] == addedConsignTest['ProjectPart'])
            pytest.assume(res['rows'][0]['WitnessPerson'] == addedConsignTest['WitnessPerson'])
            TestSampleMasterID = res['rows'][0]['ID']


        with allure.step("查看外委试验详情"):
            resp = consignExperimentDetectionManagementObj.getConsignTestById(TestSampleMasterID)

            # 断言
            pytest.assume(resp['data']['ConsignNo'] == addedConsignTest['ConsignNo'])
            pytest.assume(resp['data']['ReportNo'] == addedConsignTest['ReportNo'])
            pytest.assume(resp['data']['SampleName'] == addedConsignTest['SampleName'])
            pytest.assume(resp['data']['ProjectPart'] == addedConsignTest['ProjectPart'])
            pytest.assume(resp['data']['WitnessPerson'] == addedConsignTest['WitnessPerson'])

        with allure.step("新增检测参数"):
            consignExperimentDetectionManagementObj.createConsignTestDetail(TestSampleMasterID,ConsignTestClsID)

        with allure.step("获取检测参数列表"):
            resp = consignExperimentDetectionManagementObj.getConsignTestDetailList(TestSampleMasterID)
            ID = resp['rows'][0]['ID']
            pytest.assume(len(resp['rows']) > 0)

        with allure.step("删除检测参数信息"):
            resp = consignExperimentDetectionManagementObj.deleteConsignTestDetail(ID)

            # 断言
            pytest.assume(resp['state'] == 1)


        with allure.step("删除外委试验"):
            consignExperimentDetectionManagementObj.deleteAddedConsignTest(TestSampleMasterID)


if __name__ == '__main__':
    pytest.main(['test_experiment_detection_scan.py','-s'])

