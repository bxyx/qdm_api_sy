# -*- coding: UTF-8  -*-
# @time     : 2023-10-08 15:39
# @Author   : Zhong Xue
# @File     : conftest.py
import time

import pytest

from api.experiment_detection_management.consign_experiment_detection_management import \
    consignExperimentDetectionManagementObj
from api.experiment_detection_management.experiment_detection import experimentDetectionObj
from api.sample_management.sample_master_management import sampleMasterManagementObj
from common.mySetting import current_env
from common.public import ranStr, getPassTime


@pytest.fixture(scope='function')
def fixtureCreateExperiment(fixtureGetLoginUserInfo,
                            fixtureCreateSampleMaster,
                            fixtureGetMaterialTypeProjectTree):
    ssoName = fixtureGetLoginUserInfo['ssoName']
    ContractSectID = fixtureCreateSampleMaster['ContractSectID']
    SampleCode = fixtureCreateSampleMaster['SampleCode']
    testItemId = fixtureGetMaterialTypeProjectTree.get(current_env['materialType'],'')

    # 获取试验样品
    testProjectTree = experimentDetectionObj.getBuildTestProjectTree()

    materialTypeID = ''
    for k, v in testProjectTree.items():
        if v[0] == testItemId:
            materialTypeID = v[1]['ID']
            break

    if testItemId != '':

        # 查询待创建试验的样品
        sampleMasterList = experimentDetectionObj.getAllSampleMaster(ContractSectID, materialTypeID, SampleCode)
        sampleId = sampleMasterList['rows'][0]['ID']

        # 创建试验
        experimentDetectionObj.relationTestSample(sampleId, testItemId)
        resp = experimentDetectionObj.createTest(ssoName, ContractSectID, testItemId, sampleId, SampleCode, 1)
        TaskID = resp['TaskID']
        experimentDetectionObj.updateTaskID(TaskID, sampleId)
    else:
        resp = experimentDetectionObj.getBuildTestProjectTree()
        testItemId = resp.get(current_env['materialType'])[0]

        # 直接创建试验
        resp = experimentDetectionObj.createTest(ssoName, ContractSectID, testItemId, QualificationType=1,
                                                 isRelateSample=False)
        TaskID = resp['TaskID']

    resp = experimentDetectionObj.getTestTaskListV2(ssoName, ContractSectID, testItemId, TaskID)

    experimentInfoDict = dict()
    experimentInfoDict['uuid'] = resp['rows'][0]['uuid']
    experimentInfoDict['TaskID'] = resp['rows'][0]['TaskID']

    return experimentInfoDict

@pytest.fixture(scope='class')
def fixtureCreateConsignTest(request):
    """创建外委试验(第三方试验)类型"""
    # 获取统计标签
    resp = consignExperimentDetectionManagementObj.getConsignTestAllTables()
    testTables = resp['result'][0]

    # 获取结果类型
    resp = consignExperimentDetectionManagementObj.getConsignTestResultType()
    testResultTables = resp['result'][0]


    markers = request.node.get_closest_marker('pass_data')
    isThirdTest = markers.kwargs.get('isThirdTest')
    DataType = markers.kwargs.get('DataType')

    # 创建试验类型
    data = consignExperimentDetectionManagementObj.createConsignTestCls(testResultTables['Id'], testResultTables['Id'], testTables['Name'], isThirdTest, DataType)

    resp = consignExperimentDetectionManagementObj.getConsignTestTree(isThirdTest=isThirdTest,DataType=DataType)

    consignTestTreeId = ''
    for one in resp['data']:
        if one['text'] == data['Name']:
            consignTestTreeId = one['id']
            break
    data['consignTestTreeId'] = consignTestTreeId
    yield data
    # 查询新增的外委试验类型
    consignExperimentDetectionManagementObj.deleteConsignTest(consignTestTreeId)
