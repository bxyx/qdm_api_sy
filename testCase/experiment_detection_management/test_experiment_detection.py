# -*- coding: UTF-8  -*-
# @time     : 2023-10-08 15:30
# @Author   : Zhong Xue
# @File     : test_experiment_detection.py
import os

import allure
import pytest
import xlrd

from api.baseApi import APILogIn
from api.experiment_detection_management.experiment_detection import experimentDetectionObj, ExperimentDetection
from api.experiment_query_management.experiment_query import experimentQueryObj
from common.mySetting import current_env, externalFilePath


@allure.epic("QDM_API")
@allure.feature("试验检测管理")
@allure.story("试验检测")
@pytest.mark.pass_menu_address('/web/TestTask.html')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestExperimentDetection:
    """测试试验检测"""

    @allure.title("测试创建试验")
    @pytest.mark.project_param(IsOpenSign="是否开启电子签名",IsOpenVerify="是否开启审核")  # gxjxpre 此系统控制参数 ParamCode=IsElectronSign
    def test_create_experiment_001(self,fixtureCreateSampleMaster,
                                   fixtureGetMaterialTypeProjectTree,
                                   fixtureGetLoginUserInfo,
                                   fixtureGetProjectParamValue):
        """测试创建试验"""
        ssoName = fixtureGetLoginUserInfo['ssoName']
        userId = fixtureGetLoginUserInfo['userId']
        ContractSectID = fixtureCreateSampleMaster['ContractSectID']
        SampleCode = fixtureCreateSampleMaster['SampleCode']
        testItemId = fixtureGetMaterialTypeProjectTree.get(current_env['materialType'],'')

        if testItemId != '':
            with allure.step("获取试验样品信息"):
                testProjectTree = experimentDetectionObj.getBuildTestProjectTree()

            materialTypeID = ''
            for k,v in testProjectTree.items():
                if v[0] == testItemId:
                    materialTypeID = v[1]['ID']
                    break

            with allure.step("查询待创建试验样品信息"):
                sampleMasterList = experimentDetectionObj.getAllSampleMaster(ContractSectID, materialTypeID, SampleCode)

                # 断言
                pytest.assume(sampleMasterList['rows'][0]['SampleCode'] == SampleCode)
                sampleId = sampleMasterList['rows'][0]['ID']

            with allure.step("创建试验"):
                experimentDetectionObj.relationTestSample(sampleId, testItemId)
                resp = experimentDetectionObj.createTest(ssoName, ContractSectID, testItemId, sampleId, SampleCode, 1)
                TaskID = resp['TaskID']
                experimentDetectionObj.updateTaskID(TaskID,sampleId)

        else:
            resp = experimentDetectionObj.getBuildTestProjectTree()
            testItemId = resp.get(current_env['materialType'])[0]

            # 直接创建试验
            resp = experimentDetectionObj.createTest(ssoName, ContractSectID, testItemId, QualificationType=1, isRelateSample=False)
            TaskID = resp['TaskID']

        with allure.step("查询新增的试验信息"):
            resp = experimentDetectionObj.getTestTaskListV2(ssoName, ContractSectID, testItemId, TaskID)
            srcSampleID = ''
            flag = 0
            # 断言
            for one in resp['rows']:
                if one['TaskID'] == TaskID:
                    srcSampleID = one['SampleID']
                    flag = 1
                    break
            pytest.assume(flag == 1)
        with allure.step("复制试验"):
            copyTaskID = experimentDetectionObj.copyTestTask(userId, ssoName, TaskID)

        with allure.step("查询复制的试验信息"):
            resp = experimentDetectionObj.getTestTaskListV2(ssoName, ContractSectID, testItemId, copyTaskID)

            copySampleID = ''
            # 断言
            for one in resp['rows']:
                if one['TaskID'] == copyTaskID:
                    copySampleID = one['SampleID']
                    break

            pytest.assume(copySampleID.strip('') == srcSampleID.strip(''))

        with allure.step("删除试验任务"):
            resp = experimentDetectionObj.deleteTestTask(ssoName,copyTaskID)

            # 断言
            pytest.assume(resp == True)

        with allure.step("完成"):
            isAuditOrSign = True if (fixtureGetProjectParamValue.get('是否开启审核') == '1' or fixtureGetProjectParamValue.get('是否开启电子签名') == '1' or fixtureGetProjectParamValue.get('是否开启电子签名') == '2') else False
            experimentDetectionObj.commitMaterialInputInfo(TaskID,isAuditOrSign)

        with allure.step("查询试验信息"):
            resp = experimentDetectionObj.getTestTaskListV2(ssoName, ContractSectID, testItemId, TaskID, 1, "2,4,5,7,9")

            flag = 0
            # 断言
            for one in resp['rows']:
                if one['TaskID'] == TaskID:
                    ForeignTable = one['ForeignTable']
                    if fixtureGetProjectParamValue.get('是否开启审核') == '1':
                        pytest.assume(one['TaskStateDesc'] == '审核中')

                    elif fixtureGetProjectParamValue.get('是否开启电子签名') == '1' or fixtureGetProjectParamValue.get('是否开启电子签名') == '2':
                        pytest.assume(one['TaskStateDesc'] == '签字中')

                    else:
                        pytest.assume(one['TaskStateDesc'] == '已完成')
                    flag = 1
                    break
            pytest.assume(flag == 1)

        with allure.step("撤销试验"):
            experimentDetectionObj.execCancelWorkFlow(TaskID,ForeignTable)

        with allure.step("删除试验"):
            experimentDetectionObj.deleteTestTask(ssoName, TaskID)



    @allure.title("测试任务转交")
    def test_experiment_handover_001(self, fixtureCreateExperiment,
                                   fixtureGetLoginUserInfo,
                                   fixtureGetMaterialTypeProjectTree):
        """测试任务转交"""
        ssoName = fixtureGetLoginUserInfo['ssoName']
        userId = fixtureGetLoginUserInfo['userId']
        labId = fixtureGetLoginUserInfo['laboratoryID']
        ContractSectID = fixtureGetLoginUserInfo['contractId']
        taskID = fixtureCreateExperiment['TaskID']
        taskUUID = fixtureCreateExperiment['uuid']
        testItemId = fixtureGetMaterialTypeProjectTree.get(current_env['materialType'],'')

        with allure.step("通过任务ID查询试验信息"):
            resp = experimentQueryObj.getTestList(labId, ContractSectID, taskID)

            # 断言
            pytest.assume(resp['rows'][0]['TaskID'] == taskID)

        with allure.step("查看日志"):
            resp = experimentQueryObj.getLogs(taskID)

            # 断言
            pytest.assume(resp['data'][0]['TaskID'] == taskID)
            pytest.assume(resp['data'][0]['Declaration'] == "创建试验")

        with allure.step("导出试验数据"):
            resp = experimentQueryObj.exportTestTask(labId, ContractSectID, ssoName)

            with open(os.path.join(externalFilePath, '试验检测.xls'), 'wb') as f:
                f.write(resp)

            # 断言
            wb = xlrd.open_workbook(os.path.join(externalFilePath, '试验检测.xls'))
            ws = wb.sheet_by_name("试验检测记录")
            table_header = ['ID', '检测编号', '报告编号', '使用部位', '填写人', '试验项目名称', '检验类别', '报告日期', '样品信息', '是否合格', '所检参数信息','试验状态', '签章状态', '合同段']
            pytest.assume(ws.row_values(0) == table_header)
            pytest.assume(ws.nrows > 1)

            # 删除生成的文件
            os.remove(os.path.join(externalFilePath, '试验检测.xls'))


        with allure.step("打印设置"):
            resp = experimentDetectionObj.printSetUp(ssoName,'1','1')

            # 断言
            pytest.assume(resp == 'ok')

        with allure.step("查看试验日志信息"):
            resp = experimentDetectionObj.getLogs(taskID)

            # 断言
            pytest.assume(len(resp['data']) != 0)

        with allure.step("任务转交"):
            resp = experimentDetectionObj.getTestUsers(userId)
            handoverName = ''
            handoverId = ''
            for k, v in resp.items():
                if k.startswith('auto'):
                    handoverName = k
                    handoverId = v
                    break

            experimentDetectionObj.taskHandover(userId, handoverId,taskUUID)

        with allure.step("查询任务信息"):
            resp = experimentDetectionObj.getTestTaskListV2(ssoName, ContractSectID, testItemId, taskID)

            # 断言
            pytest.assume(resp['rows'][0]['TestUsers'] == handoverName)

        with allure.step('被转交人登录系统删除任务'):
            token = APILogIn(userName=handoverName)
            resp = ExperimentDetection(token).deleteTestTask(handoverName,taskID)

            pytest.assume(resp == True)


if __name__ == '__main__':
    pytest.main(['test_experiment_detection.py::TestExperimentDetection','-s'])
