# -*- coding: UTF-8  -*-
# @time     : 2023-10-18 14:58
# @Author   : Zhong Xue
# @File     : test_traverse_point.py
import allure
import pytest

from api.measurement_management.traverse_point import traversePointObj


@allure.epic("QDM_API")
@allure.feature("测量管理")
@allure.story("测量管理")
@pytest.mark.pass_menu_address('/measureMgt')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestTraversePoint:
    """测试测量管理"""
    @allure.title("测试测量管理")
    @pytest.mark.parametrize('type',[0,1])
    def test_traverse_point_001(self,fixtureGetLoginUserInfo,type):
        """测试测量管理(导线点,水准点)"""
        ContractSectID = fixtureGetLoginUserInfo['contractId']
        with allure.step("新增"):
            res = traversePointObj.getDictByType("水准点等级")
            traversePointGrade = res[0]['Id']
            addedTestSiteData = traversePointObj.addTestSiteData(ContractSectID, traversePointGrade,type)

        with allure.step("查询新增的信息"):
            resp = traversePointObj.getTestSiteList(ContractSectID,addedTestSiteData['Name'],type)

            testSiteID = resp['data']['rows'][0]['ID']
            # 断言
            pytest.assume(resp['data']['rows'][0]['Name'] == addedTestSiteData['Name'])

        with allure.step("删除信息"):
            traversePointObj.deleteTestSite(testSiteID)

        with allure.step("查询信息"):
            resp = traversePointObj.getTestSiteList(ContractSectID,addedTestSiteData['Name'],type)

            # 断言
            pytest.assume(len(resp['data']['rows']) == 0)

if __name__ == '__main__':
    pytest.main(['test_traverse_point.py','-s'])