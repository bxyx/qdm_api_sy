# -*- coding: UTF-8  -*-
# @time     : 2023-10-18 10:29
# @Author   : Zhong Xue
# @File     : test_experiment_container_setting.py
import allure
import pytest

from api.experiment_parameter_setting.experiment_container_setting import experimentContainerSettingObj


@allure.epic("QDM_API")
@allure.feature("试验参数设置")
@allure.story("试验容器设置")
@pytest.mark.pass_menu_address('/web/TestParamMgr.html')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestExperimentContainerSetting:
    """测试试验容器设置"""

    @allure.title('设置试验容器设置')
    @pytest.mark.parametrize("MainType",["盒质量", "试筒", "环刀质量", "比重瓶"])
    def test_experiment_container_setting_001(self, MainType, fixtureGetLoginUserInfo):
        """测试试验容器设置"""
        labId = fixtureGetLoginUserInfo['laboratoryID']
        with allure.step("新增试验容器"):
            addedFormTestParam = experimentContainerSettingObj.addFormTestParam(labId, MainType)

        with allure.step('获取新增的试验容器'):
            addedFormTestParamList = experimentContainerSettingObj.getParamList(labId)

            flag = 0
            ParamsID = ''
            # 断言
            for one in addedFormTestParamList['rows']:
                if one['MainType'] == MainType:
                    if one['ChildType'] == addedFormTestParam['ChildType']:
                        flag = 1
                        ParamsID = one['ParamsID']
                        break

            pytest.assume(flag == 1)

        with allure.step("新增校验信息"):
            addedFormTestParamDetail = experimentContainerSettingObj.addFormTestParamDetail(ParamsID)

        with allure.step('获取新增的试验容器'):
            addedFormTestParamList = experimentContainerSettingObj.getParamList(labId)
            flag = 0
            cid = ''
            # 断言
            for one in addedFormTestParamList['rows']:
                if one['MainType'] == MainType:
                    if one['ChildType'] == addedFormTestParam['ChildType']:
                        for child in one['detaillist']:
                            if child['TypeValueA'] == addedFormTestParamDetail['TypeValueA']:
                                cid = child['ID']
                                flag = 1
                                break
            pytest.assume(flag == 1)

        with allure.step("删除校验信息"):
            resp = experimentContainerSettingObj.deleteTestDetailParam(cid)
            pytest.assume(resp == 1)

        with allure.step("删除试验容器信息"):
            resp = experimentContainerSettingObj.deleteTestParam(ParamsID)

            pytest.assume(resp == 1)




if __name__ == '__main__':
    pytest.main(['test_experiment_container_setting.py','-s'])

