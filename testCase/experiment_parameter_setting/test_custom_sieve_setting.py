# -*- coding: UTF-8  -*-
# @time     : 2023-10-18 11:13
# @Author   : Zhong Xue
# @File     : test_custom_sieve_setting.py
import allure
import pytest

from api.experiment_parameter_setting.custom_sieve_setting import customSieveSettingObj


@allure.epic("QDM_API")
@allure.feature("试验参数设置")
@allure.story("自定义筛孔设置")
@pytest.mark.pass_menu_address('/web/ShaiKongMgr.html')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestCustomSieveSetting:
    """测试自定义筛孔设置"""
    @allure.title("测试自定义筛孔设置")
    def test_custom_sieve_setting_001(self,fixtureGetLoginUserInfo):
        """测试自定义筛孔设置"""
        contractId = fixtureGetLoginUserInfo['contractId']

        with allure.step("新增自定义筛孔"):
            addedCustomSieve = customSieveSettingObj.saveSieve(contractId)

        with allure.step("获取自定义筛孔列表"):
            resp = customSieveSettingObj.getSieveList(contractId)

            flag = 0
            sieveID = ''
            for one in resp['rows']:
                if one['Name'] == addedCustomSieve['Name']:
                    flag = 1
                    sieveID = one['ID']
                    break

            pytest.assume(flag == 1)

        with allure.step("自定义筛孔明细"):
            addedSieveDetail = customSieveSettingObj.saveSieveDetail(sieveID)

        with allure.step("获取自定义筛孔明细"):
            resp = customSieveSettingObj.getSieveDetailList(sieveID)

            # 断言
            pytest.assume(resp['rows'][0]['Name'] == addedSieveDetail['Name'])

        with allure.step("删除自定义筛孔设置"):
            resp = customSieveSettingObj.deleteSieve(sieveID)

            pytest.assume(resp == 1)

if __name__ == '__main__':
    pytest.main(['test_custom_sieve_setting.py','-s'])

