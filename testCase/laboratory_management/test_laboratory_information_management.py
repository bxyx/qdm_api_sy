# -*- coding: UTF-8  -*-
# @time     : 2023-09-12 16:04
# @Author   : Zhong Xue
# @File     : test_laboratory_information_management.py
import allure
import pytest

from api.laboratory_management.laboratory_information_management import laboratoryInformationManagementObj
from common.public import ranStr, ranInt


@allure.epic("QDM_API")
@allure.feature("试验室管理")
@allure.story("试验室信息管理")
@pytest.mark.pass_menu_address('/LaboratoryInformationManage/LaboratoryInformation')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestLaboratoryInformationManagement:
    """测试试验室信息管理"""

    @allure.title("测试编辑试验室信息")
    def test_update_laboratory_information_001(self, fixtureGetLoginUserInfo):
        """编辑试验室信息"""

        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']

        with allure.step("获取试验室信息"):
            resp = laboratoryInformationManagementObj.getLaboratoryInfoByLabID(laboratoryID)
            laboratoryInfo = resp['data']
            laboratoryInfo["ApproveNo"] = ranStr(5)  # 计量认证编号
            laboratoryInfo["AuthorizePerson"] = ranStr(5)  # 授权负责人
            laboratoryInfo["DeviceAmount"] = ranInt(3) # 仪器设备(>800元)台/套
            laboratoryInfo["EMail"] = ranStr(5)  # 电子邮箱
            laboratoryInfo["EngineerTotal"] = ranInt(3)  # 员证人数
            laboratoryInfo["ExecutivePerson"] = ranStr(5)  # Auto_9F2dR
            laboratoryInfo["ExecutivePhone"] = ranInt(11)  # 行政负责人联系方式
            laboratoryInfo["Fax"] = ranInt(8)  # 传真
            laboratoryInfo["GradeNo"] = ranStr(5)  # 等级及编号
            laboratoryInfo["JuridicalPerson"] = ranStr(5)  # 法人代表
            laboratoryInfo["JuridicalPhone"] = ranInt(11)  # 法人代表联系方式
            laboratoryInfo["LabAddress"] = ranStr(5)  # 工地试验室详细地址
            laboratoryInfo["LabInUnit"] = ranStr(5)  # 工地试验室设立单位
            laboratoryInfo["LabPerson"] = ranStr(5)  # 工地试验室设立单位联系人
            laboratoryInfo["LabPhone"] = ranInt(11)  # 工地试验室设立单位联系电话
            laboratoryInfo["LabQualityPerson"] = ranStr(5)  # 质量负责人
            laboratoryInfo["LabTechPerson"] = ranStr(5)  # 技术负责人
            laboratoryInfo["NonCertTotal"] = ranInt(11)  # 非持证人数
            laboratoryInfo["OwnerPerson"] = ranStr(5)  # 项目业主单位联系人
            laboratoryInfo["OwnerPhone"] = ranInt(11)  # 项目业主单位联系电话
            laboratoryInfo["OwnerUnit"] = ranStr(5)  # 项目业主单位
            laboratoryInfo["ProjectInvest"] = ranStr(5)  # 工程投资
            laboratoryInfo["QualityPerson"] = ranStr(5)  # 质量负责人
            laboratoryInfo["QualityPhone"] = ranInt(11)  # 质量负责人联系方式
            laboratoryInfo["SurveyorTotal"] = ranInt(2)  # 师证人数
            laboratoryInfo["TechPerson"] = ranStr(5)  # 技术负责人
            laboratoryInfo["TechPhone"] = ranInt(11)  # 技术负责人联系方式
            laboratoryInfo["TelPhone"] =ranInt(11)  # 工地试验室电话
            laboratoryInfo["TestName"] =ranStr(5)  # 母体及注册人机构名称
            laboratoryInfo["TestSite"] =ranStr(5)  # 试验场地(㎡)
            laboratoryInfo["WorkSite"] =ranStr(5)  # 办公场地(㎡)
            laboratoryInfo["AuthorizeInstitution"] =ranStr(5)  # 授权机构
            laboratoryInfo["CertificateNumber"] =ranStr(5)  # 证书编号
            laboratoryInfo["AuthorizeScope"] =ranStr(5)  # 授权业务范围


        with allure.step("编辑试验室信息"):
            laboratoryInformationManagementObj.updateLaboratoryInfo(laboratoryInfo)

        with allure.step("获取试验室信息"):
            resp = laboratoryInformationManagementObj.getLaboratoryInfoByLabID(laboratoryID)

            # 断言
            pytest.assume(laboratoryInfo == resp['data'])


if __name__ == '__main__':
    pytest.main(['test_laboratory_information_management.py::TestLaboratoryInformationManagement','-s'])