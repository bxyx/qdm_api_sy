# -*- coding: utf-8 -*-
# @Time    : 2024-03-12 14:13
# @Author  : ZHONG XUE
# @Email   : 569252997@qq.com
# @File    : test_equipment_management.py
import allure
import pytest

from api.laboratory_management.equipment_management import equipmentManagementObj


@allure.epic("QDM_API")
@allure.feature("试验室管理")
@allure.story("试验设备管理")
@pytest.mark.pass_menu_address('/testDeviceManage')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestEquipmentManagement:
    """测试设备管理模块"""

    @allure.title("测试设备类型获取列表、编辑、删除")
    def test_device_type(self,fixtureAddTestDeviceType,fixtureGetLoginUserInfo):
        """测试设备类型"""
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']

        # 编辑设备类型
        resp = equipmentManagementObj.updateTestDeviceType(laboratoryID, fixtureAddTestDeviceType['id'])

        # 获取设备类型列表
        deviceTypeList = equipmentManagementObj.getTestDeviceTypeNodeList(laboratoryID).get('data')
        for one in deviceTypeList:
            if one['name'] == resp['TypeName']:
                pytest.assume(one['id'] == fixtureAddTestDeviceType['id'])

if __name__ == '__main__':
    pytest.main(['test_equipment_management.py::TestEquipmentManagement::test_device_type'])
