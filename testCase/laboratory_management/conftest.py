# -*- coding: UTF-8  -*-
# @time     : 2023-09-08 10:59
# @Author   : Zhong Xue
# @File     : conftest.py
import pytest

from api.laboratory_management.equipment_management import equipmentManagementObj
from api.laboratory_management.personnel_information_management import personnelInformationManagementObj


@pytest.fixture()
def fixtureAddPersonnelInfo(fixtureGetLoginUserInfo,request):
    """新增人员信息"""
    userId = fixtureGetLoginUserInfo['userId']
    laboratoryID = fixtureGetLoginUserInfo['laboratoryID']

    status = request.node.funcargs.get('status')

    if status is not None:
        if status == '全部':
            status = '在职'
        addUserInfo = personnelInformationManagementObj.addOrUpdatePerson(userId, laboratoryID, status)
    else:
        addUserInfo = personnelInformationManagementObj.addOrUpdatePerson(userId, laboratoryID)

    yield addUserInfo
    # 删除添加的人员信息
    personId = addUserInfo['Person']['id']
    personnelInformationManagementObj.deletePersonInfo(userId, personId)

# 设备类型
@pytest.fixture()
def fixtureAddTestDeviceType(fixtureGetLoginUserInfo):
    """新增设备类型"""
    laboratoryID = fixtureGetLoginUserInfo['laboratoryID']

    # 新增设备类型
    deviceTypeInfo = equipmentManagementObj.addTestDeviceType(laboratoryID)

    # 获取设备列表
    deviceTypeList = equipmentManagementObj.getTestDeviceTypeNodeList(laboratoryID).get('data')
    for one in deviceTypeList:
        if one['name'] == deviceTypeInfo['TypeName']:
            deviceTypeInfo['id'] = one['id']

    yield deviceTypeInfo
    # 删除设备类型
    equipmentManagementObj.deleteTestParam(deviceTypeInfo['id'])


