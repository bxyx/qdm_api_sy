# -*- coding: UTF-8  -*-
# @time     : 2023-09-08 10:58
# @Author   : Zhong Xue
# @File     : test_personnel_information_management.py
import allure
import pytest

from api.baseApi import commonAPIObj
from api.laboratory_management.personnel_information_management import personnelInformationManagementObj
from common.mySetting import reportPath

@allure.epic("QDM_API")
@allure.feature("试验室管理")
@allure.story("人员信息管理")
@pytest.mark.pass_menu_address('/biddingPersonManage/biddingPerson')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestPersonnelInformationManagement:
    """测试人员信息管理"""

    @allure.title("查询{status}人员信息")
    @pytest.mark.parametrize('status',['在职','离职','全部'])
    def test_search_personnel_info_001(self,status, fixtureAddPersonnelInfo, fixtureGetLoginUserInfo):
        """测试人员信息查询"""
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        addedPersonnelName = fixtureAddPersonnelInfo['Person']['FullName']
        with allure.step(f"查询{status}人员信息"):
            resp = personnelInformationManagementObj.getPersonList(laboratoryID, addedPersonnelName, status)

            flag = 0
            # 断言
            for one in resp['rows']:
                flag = 1
                pytest.assume(addedPersonnelName in one['FullName'])
                if addedPersonnelName == one['FullName']:
                    pytest.assume(fixtureAddPersonnelInfo['Person']['IDCardNo'] == one['IDCardNo'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['Sex'] == str(one['Sex']))
                    pytest.assume(fixtureAddPersonnelInfo['Person']['BirthDate'] == one['BirthDate'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['Dept'] == one['Dept'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['JobTitle'] == one['JobTitle'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['Hiredate'] == one['Hiredate'].split(' ')[0])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['Status'] == one['Status'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['TestWorkYear'] == one['TestWorkYear'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['Education'] == one['Education'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['GraduateSchool'] == one['GraduateSchool'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['Major'] == one['Major'])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['GraduateDate'] == one['GraduateDate'].split(' ')[0])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['ContractBeginDate'] == one['ContractBeginDate'].split(' ')[0])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['ContractEndDate'] == one['ContractEndDate'].split(' ')[0])
                    pytest.assume(fixtureAddPersonnelInfo['Person']['SocialSecurity'] == one['SocialSecurity'].split(' ')[0])

            pytest.assume(flag == 1)

    @allure.title("查看人员信息详情")
    def test_view_personnel_info_detail_001(self,fixtureAddPersonnelInfo):
        """查看人员信息详情"""
        personId = fixtureAddPersonnelInfo['Person']['id']

        with allure.step('查看人员信息详情'):
            resp = personnelInformationManagementObj.getPersonnelDetailById(personId)

            # 断言
            pytest.assume(fixtureAddPersonnelInfo['Person']['IDCardNo'] == resp['data']['IDCardNo'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['Sex'] == str(resp['data']['Sex']))
            pytest.assume(fixtureAddPersonnelInfo['Person']['BirthDate'] == resp['data']['BirthDate'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['Dept'] == resp['data']['Dept'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['JobTitle'] == resp['data']['JobTitle'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['Hiredate'] == resp['data']['Hiredate'].split(' ')[0])
            pytest.assume(fixtureAddPersonnelInfo['Person']['Status'] == resp['data']['Status'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['TestWorkYear'] == resp['data']['TestWorkYear'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['Education'] == resp['data']['Education'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['GraduateSchool'] == resp['data']['GraduateSchool'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['Major'] == resp['data']['Major'])
            pytest.assume(fixtureAddPersonnelInfo['Person']['GraduateDate'] == resp['data']['GraduateDate'].split(' ')[0])
            pytest.assume(fixtureAddPersonnelInfo['Person']['ContractBeginDate'] == resp['data']['ContractBeginDate'].split(' ')[0])
            pytest.assume(fixtureAddPersonnelInfo['Person']['ContractEndDate'] == resp['data']['ContractEndDate'].split(' ')[0])
            pytest.assume(fixtureAddPersonnelInfo['Person']['SocialSecurity'] == resp['data']['SocialSecurity'].split(' ')[0])

    @allure.title("测试人员证书: 新增、获取列表、删除")
    def test_personnel_certificate_001(self,fixtureGetLoginUserInfo,fixtureAddPersonnelInfo):
        """测试人员证书"""
        userId = fixtureGetLoginUserInfo['userId']
        personId = fixtureAddPersonnelInfo['Person']['id']
        with allure.step("新增人员证书"):
            addedPersonCert = personnelInformationManagementObj.addOrUpdatePersonCert(userId, personId)

        with allure.step('上传证书附件'):
            personnelCertId = addedPersonCert['PersonCert']['Id']
            uploadedPersonCert = commonAPIObj.uploadAccessory(personnelCertId, "PersonWorkRecord", "人员证书")

        with allure.step("查看人员证书列表"):
            resp = personnelInformationManagementObj.getPersonCertList(personId)

            # 断言
            pytest.assume(resp['data'][0]['CertNo'] == addedPersonCert['PersonCert']['CertNo'])
            pytest.assume(resp['data'][0]['CertMajor'] == addedPersonCert['PersonCert']['CertMajor'])
            pytest.assume(resp['data'][0]['CertType'] == addedPersonCert['PersonCert']['CertType'])
            pytest.assume(resp['data'][0]['IssueDate'].split(' ')[0] == addedPersonCert['PersonCert']['IssueDate'])
            pytest.assume(resp['data'][0]['MinLearnHours'] == addedPersonCert['PersonCert']['MinLearnHours'])

        with allure.step("获取人员证书附件信息"):
            resp = commonAPIObj.getAccessory(personnelCertId, "PersonWorkRecord", "人员证书")

            # 断言
            pytest.assume(resp[0]['Name'] == uploadedPersonCert['Name'])
            pytest.assume(resp[0]['Url'] == uploadedPersonCert['Url'])

        with allure.step("添加证书学时"):
            addedPersonLearnHour = personnelInformationManagementObj.addPersonLearnHours(personnelCertId)


        with allure.step("查看证书学时"):
            resp = personnelInformationManagementObj.getPersonCertHourById(personnelCertId)

            # 断言
            pytest.assume(resp['data']['CompletedLearnHours'] == addedPersonLearnHour['Hours'])
            pytest.assume(resp['data']['RemainingLearnHours'] == resp['data']['MinLearnHours'] - resp['data']['CompletedLearnHours'])

        with allure.step("获取证书学时列表"):
            resp = personnelInformationManagementObj.getPersonLearnHourListByCertId(personnelCertId)

            # 断言
            pytest.assume(resp['data'][0]['Hours'] == addedPersonLearnHour['Hours'])
            pytest.assume(resp['data'][0]['Memo'] == addedPersonLearnHour['Memo'])

        with allure.step("删除学时登记信息"):
            resp = personnelInformationManagementObj.deleteLearnHours(addedPersonLearnHour['Id'])
            # 断言
            pytest.assume(resp['state'] == 1)

        with allure.step("删除人员证书"):
            resp = personnelInformationManagementObj.deletePersonCert(userId, personnelCertId)
            # 断言
            pytest.assume(resp['state'] == 1)

    @allure.title("测试人员工作记录")
    def test_personnel_work_record_001(self, fixtureAddPersonnelInfo):
        """测试人员工作记录"""

        personId = fixtureAddPersonnelInfo['Person']['id']
        with allure.step("新增人员工作记录"):
            addedPersonnelWorkRecord = personnelInformationManagementObj.createOrUpdatePersonWorkRecord(personId)

        with allure.step("获取人员工作记录列表"):
            resp = personnelInformationManagementObj.getPersonWorkRecordById(personId)

            personnelWorkRecordId = resp['data'][0]['Id']
            # 断言
            pytest.assume(resp['data'][0]['RecordContent'] == addedPersonnelWorkRecord['RecordContent'])
            pytest.assume(resp['data'][0]['StartTime'].split(' ')[0] == addedPersonnelWorkRecord['StartTime'])
            pytest.assume(resp['data'][0]['EndTime'].split(' ')[0] == addedPersonnelWorkRecord['EndTime'])
            pytest.assume(resp['data'][0]['Workplace'] == addedPersonnelWorkRecord['Workplace'])

        with allure.step("删除人员工作记录"):
            resp = personnelInformationManagementObj.deletePersonWorkRecord(personnelWorkRecordId)

            # 断言
            pytest.assume(resp['state'] == 1)

    @allure.title("测试人员业绩")
    def test_personnel_achievement_001(self, fixtureGetLoginUserInfo, fixtureAddPersonnelInfo):
        """测试人员业绩"""

        userId = fixtureGetLoginUserInfo['userId']
        personId = fixtureAddPersonnelInfo['Person']['id']

        with allure.step('新增人员业绩'):
           addedPersionnelAchievement = personnelInformationManagementObj.addOrUpdatePersonAchievement(userId, personId)

        with allure.step("获取人员业绩列表"):
            resp = personnelInformationManagementObj.getPersonAchievement(personId)

            # 断言
            pytest.assume(resp['data'][0]['BeginDate'].split(' ')[0] == addedPersionnelAchievement['PersonAchievement']['BeginDate'])
            pytest.assume(resp['data'][0]['EndDate'].split(' ')[0] == addedPersionnelAchievement['PersonAchievement']['EndDate'])
            pytest.assume(resp['data'][0]['Post'] == addedPersionnelAchievement['PersonAchievement']['Post'])
            pytest.assume(resp['data'][0]['Project'] == addedPersionnelAchievement['PersonAchievement']['Project'])

        with allure.step("删除人员业绩列表"):
            personAchievementId = addedPersionnelAchievement['PersonAchievement']['Id']
            resp = personnelInformationManagementObj.deletePersonAchievement(userId, personAchievementId)

            # 断言
            pytest.assume(resp['state'] == 1)

    @allure.title("测试人员附件")
    def test_personnel_attachment_001(self,fixtureGetLoginUserInfo, fixtureAddPersonnelInfo):
        """测试人员附件"""
        userId = fixtureGetLoginUserInfo['userId']
        personId = fixtureAddPersonnelInfo['Person']['id']

        with allure.step("获取人员附件目录信息"):
            resp = personnelInformationManagementObj.getPersonAttachmentFolderTree(personId)
            for one in resp['data']:
                for two in one['Children']:
                    parentFoldId = two['Id']
                    break

        with allure.step("新增附件目录"):
            addedFolderInfo = personnelInformationManagementObj.addOrUpdatePersonAttachmentFolder(userId,personId,parentFoldId)

        with allure.step("获取人员附件目录信息"):
            resp = personnelInformationManagementObj.getPersonAttachmentFolderTree(personId)
            for one in resp['data']:
                for two in one['Children']:
                    if parentFoldId == two['Id']:
                        for three in two['Children']:
                            flag = 0
                            if three['Name'] == addedFolderInfo['FolderName']:
                                flag = 1
                                break
                            pytest.assume(flag == 1)
                        break

        with allure.step("删除人员附件目录"):
            resp = personnelInformationManagementObj.deletePersonAttachmentFolder(userId,addedFolderInfo['FolderId'])
            pytest.assume(resp['state'] == 1)

    @allure.title("测试人员培训登记")
    @pytest.mark.project_param(manageAuthTab = "EnablePersonManageAuthTab")
    def test_personnel_training_registration_001(self,fixtureGetLoginUserInfo,
                                                 fixtureAddPersonnelInfo,
                                                 fixtureGetProjectParamValue):
        """测试人员培训登记"""
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        personId = fixtureAddPersonnelInfo['Person']['id']
        personName = fixtureAddPersonnelInfo['Person']['FullName']

        if fixtureGetProjectParamValue['EnablePersonManageAuthTab'] != "1":
            pytest.skip("未开启系统控制参数【开启授权范围】")

        else:
            with allure.step("新增人员培训登记"):
                addedPersonTrainInfo = personnelInformationManagementObj.createPersonTrain(laboratoryID, personName, personId)

            with allure.step("获取人员培训登记"):
                resp = personnelInformationManagementObj.getPersonTrainPageList(personName)

                # 断言
                pytest.assume(resp['rows'][0]['TrainContent'] == addedPersonTrainInfo['trainContent'])
                pytest.assume(resp['rows'][0]['TrainType'] == addedPersonTrainInfo['trainType'])
                pytest.assume(resp['rows'][0]['TrainDate'].split(' ')[0] == addedPersonTrainInfo['trainDate'])
                pytest.assume(resp['rows'][0]['TrainPlace'] == addedPersonTrainInfo['trainPlace'])
                pytest.assume(resp['rows'][0]['TrainUnitOrTeacher'] == addedPersonTrainInfo['trainUnitOrTeacher'])
                pytest.assume(resp['rows'][0]['TrainPerson'] == addedPersonTrainInfo['trainPerson'])
                pytest.assume(resp['rows'][0]['TrainEffect'] == addedPersonTrainInfo['trainEffect'])

            with allure.step("删除人员培训登记信息"):
                ID = resp['rows'][0]['ID']
                resp = personnelInformationManagementObj.deletePersonTrain(ID)

                # 断言
                pytest.assume(resp['state'] == 1)

    @allure.title("测试人员工作授权")
    @pytest.mark.project_param(manageTrainTab="EnablePersonManageTrainTab")
    def test_personnel_work_authorization_001(self,fixtureAddPersonnelInfo,fixtureGetProjectParamValue):
        """测试人员工作授权"""

        personId = fixtureAddPersonnelInfo['Person']['id']
        personName = fixtureAddPersonnelInfo['Person']['FullName']

        if fixtureGetProjectParamValue['EnablePersonManageTrainTab'] != '1':
            pytest.skip("未开启系统控制参数【开启人员培训】")
        else:
            with allure.step("获取内置测试项目信息"):
                testProjectDict = personnelInformationManagementObj.getBuildTestProjectTree()

                if testProjectDict == {}:
                    pytest.xfail("授权工程项目为空")
                else:
                    for k,v in testProjectDict.items():
                        itemName = k
                        itemId = v
                        break

            with allure.step("新增人员工作授权"):
                addedPersonItemInfo = personnelInformationManagementObj.savePersonItemAuth(personId, itemId)

            with allure.step("获取工作授权列表"):
                resp = personnelInformationManagementObj.getPersonItemAuthList(personName)

                # 断言
                pytest.assume(resp['data']['PersonItemAuthList'][0]['Memo'] == addedPersonItemInfo['Memo'])
                pytest.assume(itemName in resp['data']['PersonItemAuthList'][0]['PersonItemName'])
                pytest.assume(resp['data']['PersonItemAuthList'][0]['PersonName'] == personName)

            with allure.step("删除工作授权"):
                resp = personnelInformationManagementObj.deletePersonItemAuth(resp['data']['PersonItemAuthList'][0]['Id'])

                # 断言
                pytest.assume(resp['state'] == 1)


if __name__ == '__main__':
    pytest.main(['test_personnel_information_management.py::TestPersonnelInformationManagement','-s'])