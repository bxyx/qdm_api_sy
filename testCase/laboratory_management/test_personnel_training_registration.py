# -*- coding: utf-8 -*-
# @Time    : 2024-03-11 16:03
# @Author  : ZHONG XUE
# @Email   : 569252997@qq.com
# @File    : test_personnel_training_registration.py
"""
    测试人员培训登记
"""
import allure
import pytest

from api.laboratory_management.personnel_training_registration import personnelTrainingRegistrationObj


@allure.epic("QDM_API")
@allure.feature("试验室管理")
@allure.story("人员培训登记")
@pytest.mark.pass_menu_address('/biddingPersonManage/biddingPersonTrain')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestPersonnelTrainingRegistration:
    """测试人员培训登记"""

    @allure.title("测试新增、编辑、删除人员培训登记信息")
    def test_personnel_training_registration(self,fixtureGetLoginUserInfo,fixtureAddPersonnelInfo):
        """测试新增、编辑、删除人员培训登记信息"""
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        trainPerson = fixtureAddPersonnelInfo['Person']['FullName']
        TrainPersonId = fixtureAddPersonnelInfo['Person']['id']
        with allure.step("新增人员培训登记"):
            resp = personnelTrainingRegistrationObj.createLabPersonTrain(laboratoryID,trainPerson,TrainPersonId,trainTypeName="标准规范")

        with allure.step("查询人员培训登记"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(keyWord=resp['trainContent'])

            # 断言
            for one in trainingInfo.get('rows'):
                pytest.assume(one['TrainContent'] in resp['trainContent'])
                trainingInfoID = one['ID']

        with allure.step("编辑人员培训登记"):
            resp = personnelTrainingRegistrationObj.updateLabPersonTrain(laboratoryID,trainingInfoID,trainPerson,TrainPersonId)

        with allure.step("查询人员培训登记"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(keyWord=resp['trainContent'])

            # 断言
            for one in trainingInfo.get('rows'):
                pytest.assume(one['TrainContent'] in resp['trainContent'])

        with allure.step("删除人员培训登记"):
            personnelTrainingRegistrationObj.deleteLabPersonTrain(trainingInfoID)

        with allure.step("查询人员培训登记"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(keyWord=resp['trainContent'])

            # 断言
            pytest.assume(len(trainingInfo.get('rows'))==0)

    @allure.title("测试查询人员培训登记信息")
    def test_search_personnel_training_registration(self, fixtureGetLoginUserInfo, fixtureAddPersonnelInfo):
        """测试查询人员培训登记信息"""
        laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
        trainPerson = fixtureAddPersonnelInfo['Person']['FullName']
        TrainPersonId = fixtureAddPersonnelInfo['Person']['id']

        with allure.step("新增人员培训登记"):
            resp = personnelTrainingRegistrationObj.createLabPersonTrain(laboratoryID,trainPerson,TrainPersonId,trainTypeName="标准规范")

        with allure.step("查询人员培训登记-时间查询"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(startDate=resp['trainDate'],endDate=resp['trainDate'])

            # 断言
            for one in trainingInfo.get('rows'):
                pytest.assume(one['TrainDate'].split(" ")[0] == resp['trainDate'])

        with allure.step("查询人员培训登记-所属单位查询"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(LabID=laboratoryID)

            # 断言
            for one in trainingInfo.get('rows'):
                pytest.assume(one['LabID'] == resp['labID'])

        with allure.step("查询人员培训登记-培训内容查询"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(keyWord=resp['trainContent'])

            # 断言
            for one in trainingInfo.get('rows'):
                trainingInfoID = one['ID']
                pytest.assume(resp['trainContent'] in one['TrainContent'] )

        with allure.step("查询人员培训登记-培训地点查询"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(keyWord=resp['trainPlace'])

            # 断言
            for one in trainingInfo.get('rows'):
                pytest.assume(resp['trainPlace'] in one['TrainPlace'])

        with allure.step("查询人员培训登记-培训人员查询"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(keyWord=resp['trainPerson'])

            # 断言
            for one in trainingInfo.get('rows'):
                pytest.assume(resp['trainPerson'] in one['TrainPerson'])

        with allure.step("查询人员培训登记-培训类型查询"):
            trainingInfo = personnelTrainingRegistrationObj.getLabPersonTrainList(trainType=resp['trainType'])

            # 断言
            for one in trainingInfo.get('rows'):
                pytest.assume(resp['trainType'] == str(one['TrainType']))

        with allure.step("删除人员培训登记"):
            personnelTrainingRegistrationObj.deleteLabPersonTrain(trainingInfoID)


if __name__ == '__main__':
    pytest.main(['test_personnel_training_registration.py::TestPersonnelTrainingRegistration', '-s'])