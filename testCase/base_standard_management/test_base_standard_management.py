# -*- coding: UTF-8  -*-
# @time     : 2023-09-13 15:22
# @Author   : Zhong Xue
# @File     : test_base_standard_management.py
import allure
import pytest
from api.base_standard_management.base_standard_management import baseStandardManagementObj


@allure.epic("QDM_API")
@allure.feature("规程管理")
@allure.story("试验室规程管理")
@pytest.mark.pass_menu_address('/standardListManage/standardList')
@pytest.mark.usefixtures('fixture_get_user_menu_jl')
class TestBaseStandardManagement:
    """测试规程管理"""

    @allure.title('测试规程类型和规程')
    def test_base_standard_001(self,fixtureAddStandardType):
        """测试规程类型和规程"""

        with allure.step("获取规程类型信息"):
            standardTypeList = baseStandardManagementObj.getStandardTypeList()

            flag = 0
            for one in standardTypeList['rows']:
                if one['Id'] == fixtureAddStandardType['Id']:
                    flag = 1
                    pytest.assume(str(one['OrderCode']) == fixtureAddStandardType['OrderCode'])
                    pytest.assume(one['Remark'] == fixtureAddStandardType['Remark'])
                    pytest.assume(one['TypeName'] == fixtureAddStandardType['TypeName'])
                    break

            pytest.assume(flag == 1)

        with allure.step("新增规程信息"):
            typeName = fixtureAddStandardType['TypeName']
            standardTypeId = fixtureAddStandardType['Id']
            addedStandardInfo = baseStandardManagementObj.addStandardInfo(typeName,standardTypeId)

        with allure.step("获取规程列表"):
            resp = baseStandardManagementObj.getStandardList(standardTypeId,addedStandardInfo['StandardName'])


            # 断言
            pytest.assume(resp['rows'][0]['StandardName'] == addedStandardInfo['StandardName'])
            pytest.assume(resp['rows'][0]['TypeName'] == addedStandardInfo['TypeName'])
            pytest.assume(resp['rows'][0]['PublishCode'] == addedStandardInfo['PublishCode'])
            pytest.assume(resp['rows'][0]['ExecuteDate'].split(' ')[0] == addedStandardInfo['ExecuteDate'])
            pytest.assume(resp['rows'][0]['ExpireDate'].split(' ')[0] == addedStandardInfo['ExpireDate'])
            pytest.assume(resp['rows'][0]['IsSystem'] == addedStandardInfo['IsSystem'])
            pytest.assume(resp['rows'][0]['IsDefault'] == addedStandardInfo['IsDefault'])
            pytest.assume(resp['rows'][0]['IsDefault'] == addedStandardInfo['IsDefault'])

        with allure.step("删除规程信息"):
            resp = baseStandardManagementObj.deleteStandardById(addedStandardInfo['ID'])

            # 断言
            pytest.assume(resp['state'] == 1)


if __name__ == '__main__':
    pytest.main(["test_base_standard_management.py::TestBaseStandardManagement","-s"])