# -*- coding: UTF-8  -*-
# @time     : 2023-09-13 15:26
# @Author   : Zhong Xue
# @File     : conftest.py
import pytest

from api.base_standard_management.base_standard_management import baseStandardManagementObj


@pytest.fixture(scope='module')
def fixtureAddStandardType(fixtureGetLoginUserInfo):
    """新增规程类型"""
    userId = fixtureGetLoginUserInfo['userId']
    # 新增规程类型
    addedStandardTypeInfo = baseStandardManagementObj.addOrUpdateStandardType(userId)

    yield addedStandardTypeInfo['StandarType']
    # 删除规程类型
    baseStandardManagementObj.deleteStandardType(userId, addedStandardTypeInfo['StandarType']['Id'])