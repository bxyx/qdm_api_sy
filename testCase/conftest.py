# -*- coding: UTF-8  -*-
# @time     : 2023-03-31 9:26
# @Author   : Zhong Xue
# @File     : fixture.py
import time

import pytest

from api.baseApi import commonAPIObj, CommonAPI
from api.experiment_detection_management.experiment_detection import experimentDetectionObj
from api.sample_management.material_approach_management import materialApproachManagementObj
from api.sample_management.sample_master_management import sampleMasterManagementObj
from common.log import log
from common.mySetting import current_env, company
from common.public import ranStr, getPassTime


@pytest.fixture(scope='session')
def fixtureGetLoginUserInfo():
    """获取当前登录用户信息"""
    res = commonAPIObj.getLoginUserInfo()
    yield res

@pytest.fixture(scope='function',autouse=True)
def fixture_001(request):
    log.info('*****开始执行测试用例*****: %s',request.node.nodeid.encode('utf-8').decode('unicode_escape'))
    yield
    log.info('*' * 50 + '\n')

@pytest.fixture(scope='session')
def fixtureGetProjectParamTotal():
    """获取所有的系统控制参数信息"""
    projectParamsByParamName = commonAPIObj.getProjectParam(mode="ParamName")
    projectParamsByParamCode = commonAPIObj.getProjectParam(mode="ParamCode")
    yield projectParamsByParamName,projectParamsByParamCode

@pytest.fixture(scope='function')
def fixtureGetProjectParamValue(request,fixtureGetProjectParamTotal):
    """获取系统控制参数值"""
    projectParamsByParamName,projectParamsByParamCode = fixtureGetProjectParamTotal
    marker = request.node.get_closest_marker("project_param")

    passedProjectParamDict = {}
    if marker is not None:
        for one in marker.kwargs.values():
            # 判断是否为中文
            ParamType = 'ParamName' if '\u4e00' <= one <= '\u9fa5' else 'ParamCode'

            if ParamType == "ParamName":
                ParamValue = projectParamsByParamName.get(one)
            else:
                ParamValue = projectParamsByParamCode.get(one)

            passedProjectParamDict[one] = ParamValue
    yield passedProjectParamDict


@pytest.fixture(scope='class')
def fixture_get_user_menu_jl(fixtureGetLoginUserInfo,request):
    menu_path = ''
    if request.node.get_closest_marker('pass_menu_address') is not None:
        menu_path = request.node.get_closest_marker('pass_menu_address').args[0]

    # 获取当前用户菜单
    resp = commonAPIObj.getMenuTree(fixtureGetLoginUserInfo['userId'])
    menu_dict = {}
    for one in resp['data']:
        if one['Children'] != []:
            for two in one['Children']:
                if two['Data']['IsShow']:
                    menu_dict[two['Data']['Path']] = two['Data']['DisplayName']


    # 判断是否存在该菜单,不存在则跳过
    if menu_dict.get(menu_path) is None:
        pytest.skip(reason=f"客户单位{company}不存在该功能,跳过测试用例")


@pytest.fixture(scope='session')
def fixtureGetMaterialTypeProjectTree():
    """获取材料类型树"""
    resp = sampleMasterManagementObj.getMaterialTypeProjectTree()
    materialTypeProjectTreeDict = {}
    for one in resp['data']:
        if one['Children'] == []:
            if one['Data'].get('ProjExamNameInfoDtos'):
                materialTypeProjectTreeDict[one['Text']] = one['Data']['ProjExamNameInfoDtos'][0]['ItemID']
        else:
            for two in one['Children']:
                if two['Data'].get('ProjExamNameInfoDtos'):
                    materialTypeProjectTreeDict[two['Text']] = two['Data']['ProjExamNameInfoDtos'][0]['ItemID']
    yield materialTypeProjectTreeDict


@pytest.fixture(scope='session')
def fixtureGetMaterialType(fixtureGetLoginUserInfo):
    """获取材料类型"""
    resp = materialApproachManagementObj.getMaterialType()
    materialTypeList = resp['data']
    materialType = None
    for one in materialTypeList:
        if one['Name'] == current_env['materialType']:
            materialType = one
            break

    yield materialType


@pytest.fixture(scope='function')
def fixtureCreateSampleMaster(fixtureGetLoginUserInfo,
                              fixtureGetMaterialType,
                              fixtureGetMaterialTypeProjectTree):
    """直接取样"""
    materialTypeId = fixtureGetMaterialType['ID']
    userId = fixtureGetLoginUserInfo['userId']
    laboratoryID = fixtureGetLoginUserInfo['laboratoryID']
    contractId = fixtureGetLoginUserInfo['contractId']
    testItemId = fixtureGetMaterialTypeProjectTree.get(current_env['materialType'],'')
    sampleCode = sampleMasterManagementObj.getSampleMasterNumber(contractId, testItemId)['data']
    witnessCompany=''
    witnessPerson=''
    witnessUserId=''

    sampleMasterManagementObj.createSampleMaster(userId, laboratoryID, contractId, testItemId,
                                                                    materialTypeId, None, ranStr(5), sampleCode,
                                                                    ranStr(5), "kg", ranStr(5),
                                                                    witnessCompany, witnessPerson, witnessUserId,
                                                                    ranStr(5), ranStr(5),
                                                                    getPassTime(60).split(' ')[0],
                                                                    ranStr(5), time.strftime('%Y-%m-%d'), ranStr(5),
                                                                    ranStr(5))

    sampleMasterInfo = sampleMasterManagementObj.getSampleMasterList(laboratoryID, contractId, materialTypeId, sampleCode)
    yield sampleMasterInfo['rows'][0]
    sampleMasterManagementObj.deleteSampleMaster(sampleMasterInfo['rows'][0]['ID'])


@pytest.fixture(scope='function')
def fixtureCreateExperiment(fixtureGetLoginUserInfo,
                            fixtureCreateSampleMaster,
                            fixtureGetMaterialTypeProjectTree):
    """创建试验"""
    ssoName = fixtureGetLoginUserInfo['ssoName']
    ContractSectID = fixtureCreateSampleMaster['ContractSectID']
    SampleCode = fixtureCreateSampleMaster['SampleCode']
    testItemId = fixtureGetMaterialTypeProjectTree.get(current_env['materialType'],'')

    # 获取试验样品
    testProjectTree = experimentDetectionObj.getBuildTestProjectTree()

    materialTypeID = ''
    for k, v in testProjectTree.items():
        if v[0] == testItemId:
            materialTypeID = v[1]['ID']
            break

    if testItemId != '':

        # 查询待创建试验的样品
        sampleMasterList = experimentDetectionObj.getAllSampleMaster(ContractSectID, materialTypeID, SampleCode)
        sampleId = sampleMasterList['rows'][0]['ID']

        # 创建试验
        experimentDetectionObj.relationTestSample(sampleId, testItemId)
        resp = experimentDetectionObj.createTest(ssoName, ContractSectID, testItemId, sampleId, SampleCode, 1)
        TaskID = resp['TaskID']
        experimentDetectionObj.updateTaskID(TaskID, sampleId)
    else:
        resp = experimentDetectionObj.getBuildTestProjectTree()
        testItemId = resp.get(current_env['materialType'])[0]

        # 直接创建试验
        resp = experimentDetectionObj.createTest(ssoName, ContractSectID, testItemId, QualificationType=1,
                                                 isRelateSample=False)
        TaskID = resp['TaskID']

    resp = experimentDetectionObj.getTestTaskListV2(ssoName, ContractSectID, testItemId, TaskID)

    experimentInfoDict = dict()
    experimentInfoDict['uuid'] = resp['rows'][0]['uuid']
    experimentInfoDict['TaskID'] = resp['rows'][0]['TaskID']

    return experimentInfoDict



